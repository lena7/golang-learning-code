/**
 * @Author: lena
 * @Description:斐波那契数列
 * @Version: 1.0.0
 * @Date: 2021/9/12 15:32
 */

package leecode

func Fib(n int) int {
	if n < 2 {
		return n
	}
	return Fib(n-1) + Fib(n-2)
}
