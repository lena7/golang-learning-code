/**
 * @Author: lena
 * @Description:质数
 * @Version: 1.0.0
 * @Date: 2021/9/14 20:04
 */

package leecode

// 判断是否是质数
func IsPrime(i int) bool {
	if i <= 3 {
		return i > 1
	}

	return true
}
