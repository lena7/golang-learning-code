package leecode

// ClimbStairs 73:爬楼梯
func ClimbStairs(n int) int {
	if n == 1 {
		return 1
	}
	if n == 2 {
		return 2
	}
	var data []int
	data = make([]int, n)
	data[0] = 1
	data[1] = 2
	for i := 3; i <= n; i++ {
		res := data[i-2] + data[i-3]
		data[i-1] = res
	}
	return data[len(data)-1]
}
