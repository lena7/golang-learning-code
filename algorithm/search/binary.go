/**
 * @Author: lena
 * @Description:二分查找
 * @Version: 1.0.0
 * @Date: 2021/9/12 16:27
 */

package search

func BinarySearch(arr []int, data int) int {
	i, j := 0, len(arr)-1
	mid := (i + j) / 2
	for i <= j {
		if arr[mid] == data {
			return mid
		}
		// 在左半部分
		if arr[mid] > data {
			j = mid - 1
		} else {
			// 在右半部分
			i = mid + 1
		}
		mid = (i + j) / 2
	}
	// 不存在该元素
	return -1
}
