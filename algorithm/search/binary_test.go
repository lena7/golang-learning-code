/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/12 16:38
 */

package search

import (
	"reflect"
	"testing"
)

func TestBinarySearch(t *testing.T) {
	arr := []int{2, 4, 7, 8, 10, 11, 14, 15, 18, 24, 29, 33, 36, 40, 42, 54, 88}
	index := BinarySearch(arr, 7)
	if !reflect.DeepEqual(index, 2) {
		t.Errorf("error")
	}
}

func TestBinarySearch2(t *testing.T) {
	arr := []int{2, 4, 7, 8, 10, 11, 14, 15, 18, 24, 29, 33, 36, 40, 42, 54, 88}
	type tests struct {
		find  int
		index int
	}
	test := []tests{
		{4, 1},
		{0, -1},
		{14, 6},
		{15, 7},
		{30, -1},
		{36, 12},
		{40, 13},
		{88, 16},
		{55, -1},
		{54, 15},
	}
	for _, ts := range test {
		t.Run("binary_sort", func(t *testing.T) {
			res := BinarySearch(arr, ts.find)
			if !reflect.DeepEqual(res, ts.index) {
				t.Errorf("[error] find %v res %v , but expert %v", ts.find, res, ts.index)
			}
		})
	}
}
