/**
 * @Author: lena
 * @Description:冒泡排序
 * @Version: 1.0.0
 * @Date: 2021/9/12 17:09
 */

package sort_array

func Bubbling(arr []int) {
	for i := 0; i < len(arr); i++ {
		for j := 1; j < len(arr)-i; j++ {
			if arr[j-1] > arr[j] {
				// 交换值
				arr[j-1], arr[j] = arr[j], arr[j-1]
			}
		}
	}
}
