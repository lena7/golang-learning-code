/**
 * @Author: lena
 * @Date: 2021/9/23 14:41
 * @Description: insertion_test.go
 * @Version: 1.0.0
 */

package sort_array

import (
    "reflect"
    "testing"
)

func TestInsertionSort1(t *testing.T) {
    arr := []int{12, 4, 6, 2, 8, 11, 14, 7, 88, 4, 33, 11, 24, 18, 3}
    InsertionSort1(arr)
    res := []int{2, 3, 4, 4, 6, 7, 8, 11, 11, 12, 14, 18, 24, 33, 88}
    if !reflect.DeepEqual(arr, res) {
        t.Errorf("true result is : %d",arr)
    }
}

func TestInsertionSort2(t *testing.T) {
    arr := []int{12, 4, 6, 2, 8, 11, 14, 7, 88, 4, 33, 11, 24, 18, 3}
    InsertionSort2(arr)
    res := []int{2, 3, 4, 4, 6, 7, 8, 11, 11, 12, 14, 18, 24, 33, 88}
    if !reflect.DeepEqual(arr, res) {
        t.Errorf("true result is : %d",arr)
    }
}