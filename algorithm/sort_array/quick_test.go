/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/12 16:08
 */

package sort_array

import (
	"reflect"
	"testing"
)

func TestQuickSort(t *testing.T) {
	arr := []int{12, 4, 6, 2, 8, 11, 14, 7, 88, 4, 33, 11, 24, 18, 3}
	QuickSort(arr, 0, len(arr)-1)
	res := []int{2, 3, 4, 4, 6, 7, 8, 11, 11, 12, 14, 18, 24, 33, 88}
	if !reflect.DeepEqual(arr, res) {
		t.Errorf("error")
	}
}
