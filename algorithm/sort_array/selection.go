/**
 * @Author: lena
 * @Date: 2021/9/23 16:13
 * @Description: 选择排序
 * @Version: 1.0.0
 */

package sort_array

/** 工作原理
    第一次从待排序的数据元素中选出最小（或最大）的一个元素，存放在序列的起始位置
    然后再从剩余的未排序元素中寻找到最小（大）元素，然后放到已排序的序列的末尾
    以此类推，直到全部待排序的数据元素的个数为零。
    选择排序是不稳定的排序方法。
 */

// 递减排序
func SelectionSortDown(arr []int) {
    for j:=0;j<len(arr)-1;j++ {
        max := arr[j]
        index := j
        for i:=j;i< len(arr);i++ {
            if arr[i] > max {
                max=arr[i]
                index=i
            }
        }
        // 最大值和当前值交换
        arr[index],arr[j]=arr[j],arr[index]
    }
}

// 递增排序
func SelectionSortUp(arr []int) {
    for j:=0;j<len(arr)-1;j++ {
        min := arr[j]
        index := j
        for i:=j;i< len(arr);i++ {
            if arr[i] < min {
                min=arr[i]
                index=i
            }
        }
        // 最大值和当前值交换
        arr[index],arr[j]=arr[j],arr[index]
    }
}