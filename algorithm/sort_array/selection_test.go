/**
 * @Author: lena
 * @Date: 2021/9/23 16:24
 * @Description: selection_test.go
 * @Version: 1.0.0
 */

package sort_array

import (
    "reflect"
    "testing"
)

func TestSelectionSortDown(t *testing.T) {
    arr := []int{12, 4, 6, 2, 8, 11, 14, 7, 88, 4, 33, 11, 24, 18, 3}
    SelectionSortDown(arr)
    res := []int{88,33,24,18,14,12,11,11,8,7,6,4,4,3,2}
    if !reflect.DeepEqual(arr, res) {
        t.Errorf("true result is : %d",arr)
    }
}

func TestSelectionSortUp(t *testing.T) {
    arr := []int{12, 4, 6, 2, 8, 11, 14, 7, 88, 4, 33, 11, 24, 18, 3}
    SelectionSortUp(arr)
    res := []int{2, 3, 4, 4, 6, 7, 8, 11, 11, 12, 14, 18, 24, 33, 88}
    if !reflect.DeepEqual(arr, res) {
        t.Errorf("true result is : %d",arr)
    }
}