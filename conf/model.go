package conf

type Config struct {
	Global Global `yaml:"global" mapstructure:"global"`
	User   User   `yaml:"user" mapstructure:"user"`
}

type User struct {
	ID        int        `yaml:"id" mapstructure:"id"`
	Name      string     `yaml:"name" mapstructure:"name"`
	Organizes []Organize `yaml:"organize" mapstructure:"organize"`
}

type Organize struct {
	Name  string `yaml:"name" mapstructure:"name"`
	Duty  string `yaml:"duty" mapstructure:"duty"`
	Start string `yaml:"start" mapstructure:"start"`
}

type Global struct {
	Namespace string `yaml:"namespace" mapstructure:"namespace"`
	EnvName   string `yaml:"env_name" mapstructure:"env_name"`
}
