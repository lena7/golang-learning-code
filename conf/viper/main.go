package main

import (
	"fmt"

	"github.com/spf13/viper"

	"helloworld/conf"
)

// go get "github.com/spf13/viper"
func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath("./conf")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
	conf := conf.Config{}
	err = viper.Unmarshal(&conf)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", conf)
}
