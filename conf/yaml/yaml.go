package main

import (
	"fmt"
	"sync"

	"github.com/toolkits/file"
	"gopkg.in/yaml.v2"

	"helloworld/conf"
)

func main() {
	filePath := "/Users/lena/GoWork/golang/golang-learning-code/conf/config.yaml"
	// 判断文件是否存在
	if !file.IsExist(filePath) {
		panic("不存在该yaml文件")
	}
	// 读取文件内容转换为string
	contentStr, err := file.ToTrimString(filePath)
	if err != nil {
		panic(err)
	}
	// 解析字符串
	config := conf.Config{}
	err = yaml.Unmarshal([]byte(contentStr), &config)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v", config)
}

func Yaml1() {
	data := `
	systemMap:
		k1: v1
		k2: v2
`
	type systemMap struct {
		K1  string `yaml:"k_1"`
		ck2 string `yaml:"k_2"`
	}
	type config struct {
		systemMap sync.Map `yaml:"system_map"`
	}
	conf := config{}
	err := yaml.Unmarshal([]byte(data), conf)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v", conf)
}
