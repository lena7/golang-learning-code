package msgpack

import (
	"fmt"
	"testing"

	"github.com/vmihailenco/msgpack/v5"
)

type projApp struct {
	ProjectID int `json:"project_id"`
	AppID     int `json:"app_id"`
	EnvID     int `json:"env_id"`
}

func Test_msgpack(t *testing.T) {
	data := projApp{ProjectID: 11667, AppID: 99397, EnvID: 76600}
	dataByte, err := msgpack.Marshal(data)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(dataByte))

	str := "\x83\xa9ProjectID\xce\x00\x00\x16N\xa5AppID\xce\x00\x01\xdb\x9d\xa5EnvID\xce\x00\x00hg"
	str1 := "\x83\xa9ProjectID\xce\x00\x00\x16N\xa5AppID\xce\x00\x01\xdb\x9c\xa5EnvID\xce\x00\x00hg"
	fmt.Println(str)
	fmt.Println(str1)

	res := projApp{}
	err = msgpack.Unmarshal([]byte(str1), &res)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", res)
}
