/**
 * @Author: lena
 * @Date: 2021/9/24 19:14
 * @Description: 稀疏数组，不是记录所有位置的值，只记录了有值的位置 [row,col,value]
 * @Version: 1.0.0
 */

package data_structure

// 定义稀疏数组结构
type sparse struct {
    row int // 行
    col int // 列
    value int   // 值
}

// 定义总结构
type array struct {
    s []sparse
    x int
    y int
}

// 将稀疏数组转为二维数组
func ToArray(arr array) [][]int {
    res:=[][]int{}
    res=make([][]int,arr.x)
    //res:=make([][]int,arr.x,arr.y)  // TODO：数组越界：去看尚硅谷视频是怎么写的。怎么创建一个固定大小的二维数组
    for i:=0;i<arr.x;i++ {
        r:=arr.s[i].row
        c:=arr.s[i].col
        v:=arr.s[i].value
        res[r][c]=v
    }
    return res
}