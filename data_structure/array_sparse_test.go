/**
 * @Author: lena
 * @Date: 2021/9/24 19:20
 * @Description: array_sparse_testq.go
 * @Version: 1.0.0
 */

package data_structure

import (
    "reflect"
    "testing"
)

func TestToArray(t *testing.T) {
    a:=[]sparse{
        {0,0,10},
        {1,1,20},
        {2,2,30},
        {3,3,40},
        {4,4,50},
    }
    arr:=array{a,5,5}
    expert := [][]int{
        {10,0,0,0,0},
        {0,20,0,0,0},
        {0,0,30,0,0},
        {0,0,0,40,0},
        {0,0,0,0,50},
    }
    res := ToArray(arr)
    if !reflect.DeepEqual(res,expert) {
        t.Errorf("res is %d",expert)
    }
}