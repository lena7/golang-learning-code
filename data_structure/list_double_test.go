/**
 * @Author: lena
 * @Date: 2021/10/9 16:11
 * @Description: list_double_test.go
 * @Version: 1.0.0
 */

package data_structure

import (
    "reflect"
    "testing"
)

func TestDoubleListNode(t *testing.T) {
    head:=NewDoubleListNode(0)
    p:=head
    for i:=1;i<10;i++ {
        node:=NewDoubleListNode(i)
        p.next=node
        p=p.next
    }
    ints := each(head)
    res:=[]int{0,1,2,3,4,5,6,7,8,9}
    if !reflect.DeepEqual(res,ints) {
        t.Errorf("error")
    }
}
