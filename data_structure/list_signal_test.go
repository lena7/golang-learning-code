/**
 * @Author: lena
 * @Date: 2021/9/24 20:12
 * @Description: list_tes.go
 * @Version: 1.0.0
 */

package data_structure

import (
    "fmt"
    "testing"
)

func TestList(t *testing.T) {
    head:=newListNode(1)
    head.next=newListNode(2)
    fmt.Println("start:",list(head))
    _,head=add(head,newListNode(3))
    fmt.Println("add 3:",list(head))
    _,head=delete(head,head.next)
    fmt.Println("del 2:",list(head))
    node:=newListNode(4)
    _,head=add(head,node)
    fmt.Println("add 4:",list(head))
    new:=newListNode(10)
    _,head=updata(head,node,new)
    fmt.Println("update 4->10:",list(head))
    _,head=updata(head,newListNode(1),newListNode(100))
    fmt.Println("updata noExist:",list(head))
    _, listNode := findPre(head, new)
    fmt.Println("find 10 pre:",listNode.val)
    _,head=delete(head,head)
    fmt.Println("del head:",list(head))
}
