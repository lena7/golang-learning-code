/**
 * @Author: lena
 * @Date: 2021/9/23 20:55
 * @Description: queue_test.go
 * @Version: 1.0.0
 */

package data_structure

import (
    "fmt"
    "reflect"
    "testing"
)

func TestIsEmpty(t *testing.T) {
    queue := NewQueue(5)
    if !reflect.DeepEqual(queue.isEmpty(),true) {
        t.Errorf("false")
    }
    queue.put(1)
    if !reflect.DeepEqual(queue.isEmpty(),false) {
        t.Errorf("false")
    }
}

func TestIsFull(t *testing.T) {
    queue := NewQueue(5)
    queue.put(1)
    queue.put(2)
    queue.put(3)
    queue.put(4)
    queue.put(5)
    if !reflect.DeepEqual(queue.isFull(),true) {
        t.Errorf("false")
    }
}

func TestListAll(t *testing.T) {
    queue := NewQueue(5)
    queue.put(1)
    queue.put(2)
    queue.put(3)
    queue.put(4)
    queue.put(5)
    expert := []int{1,2,3,4,5}
    res := queue.list()
    if !reflect.DeepEqual(res,expert) {
        t.Errorf("result is %d\n",res)
    }
}

func TestPop(t *testing.T) {
    queue := NewQueue(5)
    queue.put(1)
    queue.put(2)
    queue.put(3)
    queue.put(4)
    queue.put(5)
    tests:=[]int{1,2,3,4,5}
    for _, ts := range tests {
        t.Run("pop", func(t *testing.T) {
            res,_ := queue.pop()
            if !reflect.DeepEqual(res,ts) {
                t.Errorf("result is %d,not %d\n",res,ts)
            }
        })
    }
    if !reflect.DeepEqual(queue.isEmpty(),true) {
        t.Errorf("false")
    }
    if !reflect.DeepEqual(queue.isFull(),false) {
        t.Errorf("false")
    }
}

func TestQueue(t *testing.T) {
    queue := NewQueue(5)
    queue.put(1)    //[1]
    queue.put(2)    //[1,2]
    queue.put(3)    //[1,2,3]
    //queue.list()    // [1,2,3]
    queue.pop()          //[0,2,3]
    queue.put(4)    //[0,2,3,4,0]
    queue.put(5)    //[0,2,3,4,5]
    //queue.list()    // [2,3,4,5]
    queue.pop()          //[0,0,3,4,5]
    queue.put(7)    //[0,0,3,4,5,7]
    queue.put(8)    //[8,0,3,4,5,7]
    expert := []int{3,4,5,7,8}
    res := queue.list()
    if !reflect.DeepEqual(res,expert) {
        t.Errorf("result is %d\n",res)
    }
    flag, err := queue.put(9)
    fmt.Println("err:",err) // err: queue is full
    if !reflect.DeepEqual(flag,false) {
        t.Errorf("error")
    }
}