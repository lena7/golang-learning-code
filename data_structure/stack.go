/**
 * @Author: lena
 * @Date: 2021/9/23 16:32
 * @Description: 用数组实现栈
 * @Version: 1.0.0
 */

package data_structure

type Stack struct {
    size int    // 栈能存储的容量
    top int     // 栈顶
    data []interface{}  // 栈内元素
}

// 创建栈
func NewStack(size int) *Stack {
    return &Stack{
        size: size,
        top: 0,
        data: make([]interface{},size),
    }
}

// 栈满
func (this *Stack)IsFull() bool {
    // 当top=数组最大长度,说明栈满
    return this.top == this.size
}

//栈空
func (this *Stack)IsEmpty() bool {
    return this.top == 0
}

// 出栈
func (this *Stack)Pop() (interface{},bool) {
    // 栈空
    if this.IsEmpty() {
        return 0,false
    }
    // 栈顶指针向下移动
    this.top--
    //fmt.Println("出栈：",this.data[this.top])
    return this.data[this.top],true
}

// 入栈
func (this *Stack)Push(d interface{}) bool {
    // 栈满
    if this.IsFull() {
        return false
    }
    this.data[this.top]=d
    this.top++
    //fmt.Println("入栈：",d)
    return true
}

// 获取栈顶元素但不出栈
func (this *Stack)Peek() (interface{},bool) {
    // 栈空
    if this.IsEmpty() {
        return 0,false
    }
    return this.data[this.top-1],true
}

// 栈内元素
func (this *Stack)Len() int {
    return this.top
}