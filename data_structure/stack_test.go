/**
 * @Author: lena
 * @Date: 2021/9/23 21:53
 * @Description: stack_test.go
 * @Version: 1.0.0
 */

package data_structure

import (
    "reflect"
    "testing"
)
func TestStack(t *testing.T) {
    stack:= NewStack(3)
    if !reflect.DeepEqual(stack.IsEmpty(),true) {
        t.Errorf("error")
    }
    stack.Push("zh")
    stack.Push("-")
    if !reflect.DeepEqual(stack.IsEmpty(),false) {
        t.Errorf("error")
    }
    if !reflect.DeepEqual(stack.IsFull(),false) {
        t.Errorf("error")
    }
    stack.Push("ku")
    if !reflect.DeepEqual(stack.Len(),3) {
        t.Errorf("len:%d",stack.Len())
    }
    if !reflect.DeepEqual(stack.IsFull(),true) {
        t.Errorf("error")
    }
    res,_:=stack.Pop()
    if !reflect.DeepEqual(res,"ku") {
        t.Errorf("error")
    }
    res,_=stack.Pop()
    if !reflect.DeepEqual(res,"-") {
        t.Errorf("error")
    }
    res,_=stack.Pop()
    if !reflect.DeepEqual(res,"zh") {
        t.Errorf("error")
    }
}