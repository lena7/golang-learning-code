package mongo_driver

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
)

// InsertOne 添加一条记录
func InsertOne(stu Student) {
	one, err := stuColl.InsertOne(context.TODO(), stu)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("insert one success :", one.InsertedID)
}

// InsertMany 添加多条记录
func InsertMany(stu []interface{}) {
	// InsertMany 所需的第二个参数是interface{}型切片
	many, err := stuColl.InsertMany(context.TODO(), stu)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("insert many success :", many.InsertedIDs)
}

// UpdateOne 更新一个文档 若有多个匹配 只更新第一个
func UpdateOne(filter interface{}, condition interface{}) {
	one, err := stuColl.UpdateOne(context.TODO(), filter, condition)
	if err != nil {
		{
			log.Fatal(err)
		}
	}
	fmt.Println("update match:", one.MatchedCount, "; modify :", one.ModifiedCount)
}

// UpdateMany 更新多个文档
func UpdateMany(filter interface{}, condition interface{}) {
	many, err := stuColl.UpdateMany(context.TODO(), filter, condition)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("update match:", many.MatchedCount, "; modify :", many.ModifiedCount)
}

// QueryAll 查询所有
func QueryAll() {
	res := []Student{}
	find, err := stuColl.Find(context.TODO(), bson.M{})
	if err != nil {
		fmt.Println("no match data")
		return
	}
	// 记得关闭游标
	defer find.Close(context.TODO())
	// 遍历游标获取查询到的文档
	for find.Next(context.TODO()) {
		var cur Student
		// 解码find当前行 存到cur
		err = find.Decode(&cur)
		if err != nil {
			log.Fatal(err)
		}
		res = append(res, cur)
	}
	fmt.Printf("%+v\n", &res)
}

// QueryOne 查找一个文档
func QueryOne(filter interface{}) {
	var res Student
	err := stuColl.FindOne(context.TODO(), filter).Decode(&res)
	if err != nil {
		// log.Fatal(err)	// 查不到会报错
		fmt.Println("no match data")
	} else {
		fmt.Printf("%+v\n", res)
	}
}

// QueryMany 查询多个文档x
func QueryMany(filter interface{}) {
	var res []Student
	// 返回的是一个游标
	find, err := stuColl.Find(context.TODO(), filter)
	if err != nil {
		fmt.Println("no match data")
		return
	}
	// 记得关闭游标
	defer find.Close(context.TODO())
	// 遍历游标获取查询到的文档
	for find.Next(context.TODO()) {
		var cur Student
		// 解码find当前行 存到cur
		err = find.Decode(&cur)
		if err != nil {
			log.Fatal(err)
		}
		res = append(res, cur)
	}
	fmt.Printf("%+v\n", &res)
}

// DeleteOne 删除一个文档
func DeleteOne(filter interface{}) {
	one, err := stuColl.DeleteOne(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("delete count :", one.DeletedCount)
}

// DeleteMany 删除多个文档
func DeleteMany(filter interface{}) {
	many, err := stuColl.DeleteMany(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("delete count :", many.DeletedCount)
}
