package mongo_driver

import (
	"go.mongodb.org/mongo-driver/bson"
	"testing"
)

func TestQueryAll(t *testing.T) {
	QueryAll()
}

func TestQueryOne(t *testing.T) {
	filter := bson.M{"name": "lena"}
	QueryOne(filter)
}

func TestQueryMany(t *testing.T) {
	QueryMany(bson.M{"age": 13})
}

func TestInsertOne(t *testing.T) {
	QueryAll()
	stu := Student{Name: "lena", Age: 12}
	InsertOne(stu)
	QueryAll()
}

func TestInsertMany(t *testing.T) {
	stu := []interface{}{
		Student{Name: "test1", Age: 12},
		Student{Name: "test1", Age: 12},
		Student{Name: "test1", Age: 12},
		Student{Name: "test2", Age: 12},
	}
	InsertMany(stu)
}

func TestDeleteOne(t *testing.T) {
	filter := bson.M{"name": "test2"}
	QueryOne(filter)
	DeleteOne(filter)
	QueryOne(filter)
}

func TestDeleteMany(t *testing.T) {
	filter := bson.M{"name": "test1"}
	QueryOne(filter)
	DeleteMany(filter)
	QueryOne(filter)
}

func TestUpdateOne(t *testing.T) {
	condition := bson.M{
		"$set": bson.M{
			"age": 18,
		},
	}
	filter := bson.M{"name": "lena"}
	QueryMany(filter)
	UpdateOne(filter, condition)
	QueryMany(filter)
}

func TestUpdateMany(t *testing.T) {
	// 符合条件的age自增1
	condition := bson.M{
		"$inc": bson.M{
			"age": 1,
		},
	}
	filter := bson.M{"age": 12}
	QueryMany(filter)
	UpdateMany(filter, condition)
	QueryMany(bson.M{"age": 13})
}
