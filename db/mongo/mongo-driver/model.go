package mongo_driver

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Student struct {
	Id   primitive.ObjectID `bson:"_id,omitempty"`
	Name string
	Age  int
}
