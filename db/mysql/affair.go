/**
 * @Author: lena
 * @Date: 2021/10/17 20:11
 * @Description: 事务
 * @Version: 1.0.0
 */

package mysql

import "fmt"

/** 事务操作
    开启事务：func (db *DB) Begin() (*Tx, error)
    提交事务：func (tx *Tx) Commit() error
    回滚事务：func (tx *Tx) Rollback() error
 */

func Affair() {
    // 开启事务：func (db *DB) Begin() (*Tx, error)
    tx, err := db.Begin()
    if err != nil {
        // 事务已经开启
        if tx != nil {
            // 事务回滚
            tx.Rollback()
        }
        fmt.Println("tx start err :",err)
        return
    }
    sql := "update student set age = ? where id = ?"
    // 执行sql：func (tx *Tx) Exec(query string, args ...interface{}) (Result, error)
    exec, err := tx.Exec(sql, 2, 1002)
    if err != nil {
        fmt.Println("tx exec err :",err)
        tx.Rollback()
        return
    }
    // 返回受影响行数
    affected, err := exec.RowsAffected()
    if err != nil {
        fmt.Println("rows affected err :",err)
        tx.Rollback()
        return
    }
    fmt.Println("affected rows :",affected)
    // 再执行一条语句 省略是否成功的判断操作
    tx.Exec(sql, 3, 1003)
    // 提交事务：func (tx *Tx) Commit() error
    tx.Commit()
}