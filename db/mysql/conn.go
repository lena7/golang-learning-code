/**
 * @Author: lena
 * @Description:连接数据库
 * @Version: 1.0.0
 * @Date: 2021/9/12 19:47
 */

package mysql

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql" // 必须导入否则无法识别mysql
)

func ConnMysql() {
	url := "root:root@tcp(127.0.0.1:3306)/test"
	// func Open(数据库驱动名称,连接信息) (*DB, error)
	db, err := sql.Open("mysql", url)
	if err != nil {
		fmt.Println("conn err :", err)
		return
	}
	defer db.Close()
	// 尝试链接
	err = db.Ping()
	if err != nil {
		fmt.Println("ping err :", err)
		return
	}
	fmt.Println("conn success!") // conn success!
}
