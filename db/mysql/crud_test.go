/**
 * @Author: lena
 * @Description:CRUD测试代码
 * @Version: 1.0.0
 * @Date: 2021/9/12 20:39
 */

package mysql

import (
	"fmt"
	"reflect"
	"testing"
)

func TestQueryOne(t *testing.T) {
	s := QueryOne(1001)
	var expert = Student{1001, "lena", 21}
	if !reflect.DeepEqual(s, expert) {
		t.Errorf("result is %v,not expert %v", s, expert)
	}
}

func TestQueryAgeMore(t *testing.T) {
	// 查询十岁以上的学生
	s := QueryAgeMore(10)
	fmt.Println(s) // [{1001 lena 21} {1002 kity 20}]
}

func TestInsert(t *testing.T) {
	s := Student{0, "tity", 15}
	res := Insert(s)
	if !reflect.DeepEqual(res, 1005) {
		t.Errorf("res = %v", res)
	}
}

func TestUpdateOne(t *testing.T) {
	s := Student{1004, "peity", 6}
	i := UpdateOne(s)
	var e int64
	e=1
	if !reflect.DeepEqual(i, e) {
		t.Errorf("reflected row : %v", i)
	}
}

func TestDeleteOne(t *testing.T) {
	i := DeleteOne(1005)
	if !reflect.DeepEqual(i, 1) {
		t.Errorf("reflected row : %v", i)
	}
}
