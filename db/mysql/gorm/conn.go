// Package gorm 数据库操作参考：https://gorm.io/zh_CN/docs/connecting_to_the_database.html
package gorm

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// 数据库连接
var db *gorm.DB

// init 连接数据库
func init() {
	url := "root:12345678@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"
	var err error
	db, err = gorm.Open(mysql.Open(url), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	fmt.Println("conn success !")
}
