package main

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
)

var db *gorm.DB

func initDB() {
	url := "root:123456@tcp(127.0.0.1:3306)/test?charset=utf8mb4&loc=Local"
	var err error
	console := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags),
		logger.Config{
			LogLevel: logger.Info,
		})
	db, err = gorm.Open(mysql.Open(url), &gorm.Config{
		Logger: console,
	})
	if err != nil {
		panic(err)
	}
	fmt.Println("conn success !")
}

func create[T any](data []T) error {
	return db.Table("food_240525").Create(&data).Error
}
