package main

import (
	"time"
)

// version
// mysql: 5.7.18-txsql-log
func main() {
	// 初始化数据库连接
	initDB()

	now, err := time.Parse("2006-01-02 15:04:05", "2024-05-25 20:00:00")
	if err != nil {
		panic(err)
	}

	// 尝试不同类型的字段gorm插入的默认值
	// 1. type=time.Time
	f1 := []Food1{
		{Name: "exist1", Expire: now},
		{Name: "null1"},
	}
	// 时间空值插入时间零值: INSERT INTO `food1` (`name`,`expire`) VALUES ('exist','2024-05-25 20:00:00'),('null','0000-00-00 00:00:00')
	if err := create(f1); err != nil {
		panic(err)
	}

	// 2. type=*time.Time
	f2 := []Food2{
		{Name: "exist2", Expire: &now},
		{Name: "null2"},
	}
	// 时间空值插入NULL: INSERT INTO `food_240525` (`name`,`expire`) VALUES ('exist2','2024-05-26 04:00:00'),('null2',NULL)
	if err := create(f2); err != nil {
		panic(err)
	}

	// 3. type=string
	f3 := []Food3{
		{Name: "exist3", Expire: "2024-05-25 19:00:00"},
		{Name: "null3"},
	}
	// 时间空值插入字符串零值: INSERT INTO `food3` (`name`,`expire`) VALUES ('exist','2024-05-25 19:00:00'),('null','')
	if err := create(f3); err != nil {
		panic(err)
	}

	// 4. type=time.Time gorm.tag.default=null
	f4 := []Food4{
		{Name: "exist4", Expire: now},
		{Name: "null4"},
	}
	// 使用字段默认值: INSERT INTO `food_240525` (`name`,`expire`) VALUES ('exist4','2024-05-25 21:23:00.701'),('null4',DEFAULT)
	if err := create(f4); err != nil {
		panic(err)
	}
}
