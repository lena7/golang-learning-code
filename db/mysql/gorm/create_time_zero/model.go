package main

import "time"

type Food1 struct {
	Name   string
	Expire time.Time `gorm:"type:datetime"`
}

type Food2 struct {
	Name   string
	Expire *time.Time `gorm:"type:datetime"`
}

type Food3 struct {
	Name   string
	Expire string `gorm:"type:datetime"`
}

type Food4 struct {
	Name   string
	Expire time.Time `gorm:"type:datetime;default:NULL"`
}
