package gorm

import (
	"fmt"
)

func Insert(user *User) {
	res := db.Create(&user)
	if err := res.Error; err != nil {
		panic(err)
	}
	fmt.Println("success! row affected:", res.RowsAffected)
}

func Delete(user *User) {
	// 进行delete操作 是以id作为条件 所以必须保证id有值
	res := db.Delete(&user)
	if err := res.Error; err != nil {
		panic(err)
	}
	fmt.Println("success! row affected:", res.RowsAffected)
}

func QueryIn(id []string) {
	users := []User{}
	err := db.Where("id in ?", id).Find(&users).Error
	if err != nil {
		panic(err)
	}
	fmt.Printf("users:%+v\n", users)
}
