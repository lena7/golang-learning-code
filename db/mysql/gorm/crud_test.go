package gorm

import (
	"testing"
)

func TestInsert(t *testing.T) {
	Insert(&User{
		Name:  "lena",
		Age:   100,
		Grade: -1.0,
	})
}

func TestDelete(t *testing.T) {
	Delete(&User{
		ID: 1,
	})
}

func TestQueryIDs(t *testing.T) {
	ids := []string{"2", "3"}
	QueryIn(ids)
}
