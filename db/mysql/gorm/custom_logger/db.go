package main

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"time"
)

var db *gorm.DB

func initDB() {
	url := "root:123456@tcp(127.0.0.1:3306)/test?charset=utf8mb4&loc=Local"
	var err error
	db, err = gorm.Open(mysql.Open(url), &gorm.Config{
		Logger: myLogger{
			Config: logger.Config{
				LogLevel:      logger.Info,
				SlowThreshold: time.Millisecond * 50,
			},
		},
	})
	if err != nil {
		panic(err)
	}
	fmt.Println("conn success !")
}

func create[T any](data []T) error {
	return db.Create(&data).Error
}

func find() error {
	f := Food{}
	// 因为这里只是测试日志打印，就不返回结果了
	return db.Select("name").First(&f).Error
}
