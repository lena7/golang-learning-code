package main

import (
	"fmt"
	"time"
)

func main() {
	initDB()

	// 2024/05/26 19:59:08 [ERROR] sql:INSERT INTO `food_errors` (`name`,`expire`) VALUES ('error','2024-05-26 19:59:08.553'). rows:0, cost:45.7507ms. trace:helloworld
	if err := create([]FoodError{{Name: "error", Expire: time.Now()}}); err != nil {
		fmt.Println(err)
	}

	// 2024/05/26 19:57:25 [WARN] slow sql:INSERT INTO `food_240525` (`name`,`expire`) VALUES ('test','2024-05-26 19:57:25.48'). rows:1, cost:80.5482ms. trace:helloworld
	if err := create([]Food{{Name: "warn", Expire: time.Now()}}); err != nil {
		fmt.Println(err)
	}

	// 2024/05/26 20:07:20 [INFO] sql:SELECT `name` FROM `food_240525` ORDER BY `food_240525`.`name` LIMIT 1. rows:1, cost:32.1437ms. trace:helloworld
	if err := find(); err != nil {
		fmt.Println(err)
	}
}

type Food struct {
	Name   string
	Expire time.Time `gorm:"type:datetime"`
}

func (f *Food) TableName() string {
	return "food_240525"
}

type FoodError struct {
	Name   string
	Expire time.Time `gorm:"type:datetime"`
}
