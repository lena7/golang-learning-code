package gorm

type User struct {
	ID    int     `json:"id"`
	Age   int     `json:"age"`
	Name  string  `json:"name"`
	Grade float64 `json:"grade"`
}
