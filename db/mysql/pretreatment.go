/**
 * @Author: lena
 * @Date: 2021/10/17 19:11
 * @Description: sql预处理
 * @Version: 1.0.0
 */

package mysql

import (
    "fmt"
)

// 预处理方法：func (db *DB) Prepare(query string) (*Stmt, error)
// 查询单条记录sql语句预处理
func QueryOnePretreatment() {
    sqlStr := "select * from student where id = ?"
    stmt, err := db.Prepare(sqlStr)
    if err != nil {
        fmt.Println("prepare err:", err)
        return
    }
    defer stmt.Close()
    var s Student
    // 查询单行：func (s *Stmt) QueryRow(args ...interface{}) *Row {
    err = stmt.QueryRow(1001).Scan(&s.id, &s.name, &s.age)
    if err != nil {
        fmt.Println("query row err :",err)
        return
    }
    fmt.Println(s)
    // 查询不同条件，但同一个sql
    err = stmt.QueryRow(1002).Scan(&s.id, &s.name, &s.age)
    if err != nil {
        fmt.Println("query row err :",err)
        return
    }
    fmt.Println(s)
}

// 查询多条记录sql语句预处理
func QueryPretreatment() {
    sql:="select * from student where age > ?"
    stmt, err := db.Prepare(sql)
    if err != nil {
        fmt.Println("prepare err :",err)
        return
    }
    defer stmt.Close()
    // 查询所有满足条件的记录：func (s *Stmt) Query(args ...interface{}) (*Rows, error)
    rows, err := stmt.Query(10)
    if err != nil {
        fmt.Println("query err :",err)
        return
    }
    defer rows.Close()
    // 当有下一行的时候，指向下一行。若没有下一行则退出循环
    for rows.Next() {
        var s Student
        err = rows.Scan(&s.id, &s.name, &s.age)
        if err != nil {
            fmt.Println("rows scan err :",err)
            return
        }
        fmt.Println(s)
    }
}

// 更新sql语句预处理
func UpdataPretreatment() {
    sql:="update student set age = ? where id = ?"
    stmt, err := db.Prepare(sql)
    if err != nil {
        fmt.Println("prepare err :",err)
        return
    }
    defer stmt.Close()
    // 更新：func (s *Stmt) Exec(args ...interface{}) (Result, error)
    _, err = stmt.Exec(12, 1002)
    if err != nil {
        fmt.Println("update err :",err)
        return
    }
    // 第二次更新
    _, err = stmt.Exec(12, 1003)
    if err != nil {
        fmt.Println("update err :",err)
        return
    }
}