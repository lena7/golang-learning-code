package main

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
)

var client *redis.Client

// init 初始化redis连接
func init() {
	rd := redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379", // url
		Password: "",
		DB:       0, // 数据库
	})
	result, err := rd.Ping(context.Background()).Result()
	if err != nil {
		fmt.Println("ping err :", err)
		return
	}
	fmt.Println(result)
	client = rd
}

func Expire(k string) {
	err := client.Expire(context.Background(), k, time.Second*8).Err()
	if err != nil {
		panic(err)
	}
}

func Get(k string) (bool, error) {
	value, err := client.Get(context.Background(), k).Result()
	if err != nil {
		return false, err
	}
	v, err := strconv.ParseBool(value)
	if err != nil {
		panic(err)
	}
	return v, nil
}

func GetEx(k string) (bool, error) {
	value, err := client.GetEx(context.Background(), k, time.Second*30).Result()
	if err != nil {
		return false, err
	}
	v, err := strconv.ParseBool(value)
	if err != nil {
		panic(err)
	}
	return v, nil
}

func Set(k string, v bool) error {
	err := client.Set(context.Background(), k, v, 30*time.Second).Err()
	if err != nil {
		return err
	}
	return nil
}

func SAdd(key string, value []string) int64 {
	result, err := client.SAdd(context.Background(), key, value).Result()
	if err != nil {
		panic(err)
	}
	return result
}

func SMembers(key string) []string {
	result, err := client.SMembers(context.Background(), key).Result()
	if err != nil {
		panic(err)
	}
	return result
}
