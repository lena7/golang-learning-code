package main

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
)

func TestExpire(t *testing.T) {
	k := "lena_191"
	err := Set(k, true)
	if err != nil {
		panic(err)
	}
	ttl := client.TTL(context.Background(), k)
	fmt.Println("过期时间1：", ttl) // 过期时间1： ttl lena_191: 30s

	fmt.Println("等待5s后")
	time.Sleep(time.Second * 5)
	ttl = client.TTL(context.Background(), k)
	fmt.Println("过期时间2：", ttl) // 过期时间2： ttl lena_191: 25s

	fmt.Println("重设key")
	err = Set(k, true)
	if err != nil {
		panic(err)
	}
	ttl = client.TTL(context.Background(), k)
	fmt.Println("过期时间3：", ttl) // 过期时间3： ttl lena_191: 30s

	fmt.Println("等待5s后")
	time.Sleep(time.Second * 5)
	ttl = client.TTL(context.Background(), k)
	fmt.Println("过期时间4：", ttl) // 过期时间4： ttl lena_191: 25s

	fmt.Println("重新获取key:Get")
	_, err = Get(k)
	if err != nil {
		panic(err)
	}
	ttl = client.TTL(context.Background(), k)
	fmt.Println("过期时间5：", ttl) // 过期时间5： ttl lena_191: 25s

	fmt.Println("重新获取key:GetEx")
	_, err = GetEx(k)
	if err != nil {
		panic(err)
	}
	ttl = client.TTL(context.Background(), k)
	fmt.Println("过期时间6：", ttl) // 过期时间6： ttl lena_191: 30s

	fmt.Println("等待30s后")
	time.Sleep(time.Second * 30)
	ttl = client.TTL(context.Background(), k)
	fmt.Println("过期时间7：", ttl) // 过期时间7： ttl lena_191: -2ns
}

func TestSetAndGet(t *testing.T) {
	k := "lena_191"
	err := Set(k, true)
	if err != nil {
		panic(err)
	}

	b, err := Get(k)
	if err != nil {
		panic(err)
	}
	fmt.Println(k, b)

	k2 := "lena_1912"
	b2, err := Get(k2)
	if err != nil {
		if err == redis.Nil {
			fmt.Println("不存在key：", k2)
			return
		}
		panic(err)
	}
	fmt.Println(k2, b2)
}

func TestSMembers(t *testing.T) {
	key := "lena"
	value := []string{"role1", "role2", "role3"}
	fmt.Println(SAdd(key, value)) // 3
	members := SMembers(key)
	fmt.Println(members) // [role2 role3 role1]
	members = SMembers(key + "1")
	fmt.Println(members) // []
}
