## 连接
### 1、下载安装包
目前有两种redis方式支持go操作，一种是go-redis，该方式支持连接哨兵及集群模式的Redis。其包获取命令如下：
```powershell
go get -u github.com/go-redis/redis
```
还有另外一种比较常用的redigo方式，其包获取命令如下：
```powershell
 go get -u github.com/garyburd/redigo/redis
```
本文章采用第一种go-redis，若想看第二种redigo方式操作redis可以移步另一篇文章：[https://blog.csdn.net/lena7/article/details/120345051](https://blog.csdn.net/lena7/article/details/120345051)
### 2、启动redis服务
确保`redis-server.exe`和`redis-cli.exe`依次启动了
### 3、开启服务端监控信息
在`redis-cli.exe`中输入命令：`monitor`，开启监控
```powershell
127.0.0.1:6379> monitor
OK
```
开启监控后，我们的所有操作都会在redis-cli.exe中打印出来。
### 3、go连接Redis
使用go-redis连接方式：
```go
import (
    "fmt"
    "github.com/go-redis/redis"
)

func ConnRedis() {
    rd := redis.NewClient(&redis.Options{
        Addr: "127.0.0.1:6379", // url
        Password: "",
        DB:0,   // 0号数据库
    })
    result, err := rd.Ping().Result()
    if err != nil {
        fmt.Println("ping err :",err)
        return
    }
    fmt.Println(result)
}
```

执行后，`redis-cli.exe`打印了该信息：
```powershell
1631848109.771606 [0 127.0.0.1:54185] "ping"
```
### 4、简单操作redis
set操作：Set(key,value,过期时间).Err()，如有错误返回错误信息
get操作：Get(key).Result()，返回获取参数和错误信息（无错误则为nil）
注意：当get获取一个不存在的value时，错误信息会返回`redis.nil`，这时候要记得区分。
```go
// 全局变量：连接数据库
// 将redis连接获取到的`*redis.Client`对象作为全局变量，供后续操作
var rd  *redis.Client= redis.NewClient(&redis.Options{
    Addr: "127.0.0.1:6379", // url
    Password: "",
    DB:0,   // 数据库
})

// string操作
func SetAndGet() {
    // set操作：第三个参数是过期时间，如果是0表示不会过期。
    err := rd.Set("k1", "v1",0).Err()
    if err != nil {
        fmt.Println("set err :",err)
        return
    }
    // get操作
    val,err := rd.Get("k1").Result()
    if err != nil {
        fmt.Println("get err :",err)
        return
    }
    fmt.Println("k1 ==",val)    // k1 == v1
    rd.Close()	// 记得关闭连接
}
```
在`redis-cli.exe`控制台输出了监控信息：
```powershell
1631866388.351282 [0 127.0.0.1:50801] "set" "k1" "v1"
1631866388.351520 [0 127.0.0.1:50801] "get" "k1"
```
## go操作redis代码
```go
/**
 * @Author: lena
 * @Description:使用go-redis操作redis:该方式支持连接哨兵及集群模式的Redis。
 * @Version: 1.0.0
 * @Date: 2021/9/16 19:54
 */

package go_redis

import (
	"fmt"
	"github.com/go-redis/redis"
)

// 连接
func ConnRedis2() {
	rd := redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379", // url
		Password: "",
		DB:       0, // 数据库
	})
	result, err := rd.Ping().Result()
	if err != nil {
		fmt.Println("ping err :", err)
		return
	}
	fmt.Println(result)
}

// 全局变量：连接数据库
var rd *redis.Client = redis.NewClient(&redis.Options{
	Addr:     "127.0.0.1:6379", // url
	Password: "",
	DB:       0, // 数据库
})

// string操作
func SetAndGet() {
	// set操作：第三个参数是过期时间，如果是0表示不会过期。
	err := rd.Set("k1", "v1", 0).Err()
	if err != nil {
		fmt.Println("set err :", err)
		return
	}
	// get操作
	val, err := rd.Get("k1").Result()
	if err != nil {
		fmt.Println("get err :", err)
		return
	}
	fmt.Println("k1 ==", val) // k1 == v1
	// get获取一个不存在的key，err会返回redis.Nil，因此要注意判断err
	val2, err := rd.Get("k2").Result()
	if err == redis.Nil {
		fmt.Println("k2 does not exist") // k2 does not exist
	} else if err != nil {
		panic(err)
	} else {
		fmt.Println("k2", val2)
	}
	rd.Close()
}

// set操作
func Set() {
	defer rd.Close()
	// 添加元素
	rd.SAdd("key", "v1", "v2", "v3")
	// 取出全部元素
	members := rd.SMembers("key")
	for _, value := range members.Val() {
		fmt.Printf("%s\t", value) // v2	v1	v3
	}
	fmt.Println()
	// 删除某个元素
	rd.SRem("key", "v2")
	// 判断某个元素是否存在
	fmt.Println(rd.SIsMember("key", "v2").Val()) // false
	// 获取当前set集合的长度
	fmt.Println(rd.SCard("key")) // scard key: 2
}
```