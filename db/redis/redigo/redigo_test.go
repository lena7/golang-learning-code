/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/17 15:06
 */

package redigo

import (
	"fmt"
	"reflect"
	"testing"
)

func TestConnRedis1(t *testing.T) {
	ConnRedis1()
}

func TestString(t *testing.T) {
	res, err := String()
	if err != nil {
		fmt.Println("err :", err)
		return
	}
	if !reflect.DeepEqual(res, 1001) {
		t.Errorf("error")
	}
}
