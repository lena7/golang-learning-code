/**
 * @Author: lena
 * @Date: 2021/10/16 16:48
 * @Description: calculate_test.go
 * @Version: 1.0.0
 */

package exercise

import (
    "fmt"
    "reflect"
    "testing"
)

func StartCalculate(input string) int {
    res,err := calculate(input)
    if err != nil {
        fmt.Println(err)
    }
    return res
}

func Test(t *testing.T) {
    sum := StartCalculate("3*4-7")
    fmt.Println("[res]",sum)
}

func Test1(t *testing.T) {
    input:="4+5"
    res:=9
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("[%s] expect:%d,result:%d",input,res,r)
    }
}

func Test2(t *testing.T) {
    input:="4+5*2"
    res:=14
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("[%s] expect:%d,result:%d",input,res,r)
    }
}

func Test3(t *testing.T) {
    input:="3*4-7"
    res:=5
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("[%s] expect:%d,result:%d",input,res,r)
    }
}

func Test4(t *testing.T) {
    input:="(1+5)+2"
    res:=8
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("cacluate:%s expect:%d,result:%d",input,res,r)
    }
}

func Test5(t *testing.T) {
    input:="2+(1+5)*2"
    res:=14
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("cacluate:%s expect:%d,result:%d",input,res,r)
    }
}

func Test6(t *testing.T) {
    input:="2*4+(1-5)*2"
    res:=0
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("cacluate:%s expect:%d,result:%d",input,res,r)
    }
}

func Test7(t *testing.T) {
    input:="2*4+((1-5)*2-2)"
    res:=-2
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("cacluate:%s expect:%d,result:%d",input,res,r)
    }
}

func Test8(t *testing.T) {
    input:="12+423"
    res:=435
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("cacluate:%s expect:%d,result:%d",input,res,r)
    }
}

func Test9(t *testing.T) {
    input:="12+4*(5-7)"
    res:=4
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("cacluate:%s expect:%d,result:%d",input,res,r)
    }
}

func Test10(t *testing.T) {
    input:="43-40*2"
    res:=-37
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("cacluate:%s expect:%d,result:%d",input,res,r)
    }
}

func Test11(t *testing.T) {
    input:="43-40*2-(80-3)"
    res:=-114
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("cacluate:%s expect:%d,result:%d",input,res,r)
    }
}

func Test12(t *testing.T) {
    input:="(43-40)*(2-(8+3))"
    res:=-27
    r:=StartCalculate(input)
    if !reflect.DeepEqual(r,res) {
        t.Errorf("cacluate:%s expect:%d,result:%d",input,res,r)
    }
}