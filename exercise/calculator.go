/**
 * @Author: lena
 * @Description:计算器
 * @Version: 1.0.0
 * @Date: 2021/9/15 17:56
 */

package exercise

import "fmt"

func StartCalculator() {
	for {
		fmt.Println("-----------计算器-----------")
		fmt.Println("   1.加法")
		fmt.Println("   2.减法")
		fmt.Println("   3.乘法")
		fmt.Println("   4.除法")
		fmt.Println("   5.退出")
		fmt.Println("---------------------------")
		fmt.Printf("请选择：")
		choose := 0
		fmt.Scanln(&choose)
		if choose == 5 {
			break
		}
		a := 0
		b := 0
		fmt.Printf("请输入操作数1：")
		fmt.Scanln(&a)
		fmt.Printf("请输入操作数2：")
		fmt.Scanln(&b)
		res := 0
		switch choose {
		case 1:
			res = add(a, b)
		case 2:
			res = sub(a, b)
		case 3:
			res = mul(a, b)
		case 4:
			res = div(a, b)
		}
		fmt.Println("计算结果为:", res)
	}
	fmt.Println("退出计算器！")
}

func add(a, b int) int {
	return a + b
}

func sub(a, b int) int {
	return a - b
}

func mul(a, b int) int {
	return a * b
}

func div(a, b int) int {
	return a / b
}
