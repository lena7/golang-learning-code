/**
 * @Author: lena
 * @Description:定义客户实体
 * @Version: 1.0.0
 * @Date: 2021/9/16 15:00
 */

package customer_system

import "fmt"

// 定义customer结构体
type customer struct {
	id     int
	name   string
	gender string
	age    int
	phone  string
	email  string
}

func newCustomer(id int, name string, gender string, age int, phone string, email string) customer {
	return customer{id, name, gender, age, phone, email}
}

// 格式化数据
func (this customer) getInfo() string {
	s := fmt.Sprintf("%v\t%v\t%v\t%v\t%v\t%v\t", this.id, this.name, this.gender, this.age, this.phone, this.email)
	return s
}
