/**
 * @Author: lena
 * @Description:定义service层接口
 * @Version: 1.0.0
 * @Date: 2021/9/16 15:02
 */

package customer_system

type customerService interface {
	getById(id int) customer
	queryAll() map[int]customer
	insert(c customer) int
	delete(id int) int
	update(c customer) int
}
