/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/16 15:20
 */

package customer_system

// 定义customer切片,存储所有顾客和数量
type customers struct {
	data map[int]customer
	num  int
}

func newCustomers() *customers {
	data := make(map[int]customer, 10)
	return &customers{data, 0}
}

// 判断cus是否存在
func (this customers) isExist(id int) bool {
	_, exist := this.data[id]
	return exist
}

// 实现service层方法
func (this customers) getById(id int) customer {
	return this.data[id]
}

func (this customers) queryAll() map[int]customer {
	return this.data
}

func (this *customers) insert(c customer) int {
	cus := newCustomer(c.id, c.name, c.gender, c.age, c.phone, c.email)
	this.data[c.id] = cus
	return 1
}

func (this *customers) delete(id int) int {
	delete(this.data, id)
	return 1
}

func (this *customers) update(c customer) int {
	this.data[c.id] = c
	return 1
}
