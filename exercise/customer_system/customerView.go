/**
 * @Author: lena
 * @Description:操作界面
 * @Version: 1.0.0
 * @Date: 2021/9/16 15:18
 */

package customer_system

import "fmt"

var c = newCustomers()

func StartCustomerSystem() {
	for {
		fmt.Println("---------客户信息管理软件---------")
		fmt.Println("   1.添加客户")
		fmt.Println("   2.修改客户")
		fmt.Println("   3.删除客户")
		fmt.Println("   4.客户列表")
		fmt.Println("   5.查询客户")
		fmt.Println("   6.退出")
		fmt.Println("-------------------------------")
		fmt.Printf("请输入你要进行的操作：")
		choose := 0
		fmt.Scanln(&choose)
		if choose == 6 {
			break
		}
		switch choose {
		case 1:
			insert()
		case 2:
			updata()
		case 3:
			deleteCus()
		case 4:
			queryAll()
		case 5:
			getById()
		}
	}
	fmt.Println("退出客户信息管理软件！")
}

func getById() {
	fmt.Printf("请输入你要查询的客户ID：")
	id := 0
	fmt.Scanln(&id)
	customer := c.getById(id)
	fmt.Printf("id\tname\tgender\tage\tphone\temail\n")
	fmt.Println(customer.getInfo())
}

func queryAll() {
	fmt.Printf("id\tname\tgender\tage\tphone\temail\n")
	customers := c.queryAll()
	for _, cus := range customers {
		fmt.Println(cus.getInfo())
	}
}

func insert() {
	var id, age int
	var name, gender, phone, email string
	fmt.Printf("请输入要添加客户的ID：")
	fmt.Scanln(&id)
	if c.isExist(id) {
		fmt.Println("该客户ID已存在！添加失败！")
		return
	}
	fmt.Printf("请输入要添加客户的姓名：")
	fmt.Scanln(&name)
	fmt.Printf("请输入要添加客户的性别：")
	fmt.Scanln(&gender)
	fmt.Printf("请输入要添加客户的年龄：")
	fmt.Scanln(&age)
	fmt.Printf("请输入要添加客户的电话：")
	fmt.Scanln(&phone)
	fmt.Printf("请输入要添加客户的邮箱：")
	fmt.Scanln(&email)
	cus := newCustomer(id, name, gender, age, phone, email)
	res := c.insert(cus)
	if res == 1 {
		fmt.Println("添加客户成功！")
	} else {
		fmt.Println("添加失败！")
	}
}

func deleteCus() {
	fmt.Printf("请输入你要删除的客户ID：")
	id := 0
	fmt.Scanln(&id)
	res := c.delete(id)
	if res == 1 {
		fmt.Println("删除客户成功！")
	} else {
		fmt.Println("删除失败！")
	}
}

func updata() {
	var id, age int
	var name, gender, phone, email string
	fmt.Printf("请输入你要更新的客户ID：")
	fmt.Scanln(&id)
	if !c.isExist(id) {
		fmt.Println("该客户不存在，无法更新！")
		return
	}
	fmt.Printf("请输入要更新客户的姓名：")
	fmt.Scanln(&name)
	fmt.Printf("请输入要更新客户的性别：")
	fmt.Scanln(&gender)
	fmt.Printf("请输入要更新客户的年龄：")
	fmt.Scanln(&age)
	fmt.Printf("请输入要更新客户的电话：")
	fmt.Scanln(&phone)
	fmt.Printf("请输入要更新客户的邮箱：")
	fmt.Scanln(&email)
	cus := newCustomer(id, name, gender, age, phone, email)
	res := c.update(cus)
	if res == 1 {
		fmt.Println("更新客户信息成功！")
	} else {
		fmt.Println("更新失败！")
	}
}
