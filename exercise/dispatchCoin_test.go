/**
 * @Author: lena
 * @Description:分金币测试
 * @Version: 1.0.0
 * @Date: 2021/9/12 15:53
 */

package exercise

import (
	"reflect"
	"testing"
)

func TestDispatchCoin(t *testing.T) {
	_, res := DispatchCoin()
	if !reflect.DeepEqual(res, 10) {
		t.Errorf("result error")
	}
}
