/**
 * @Author: lena
 * @Description:家庭记账软件
 * @Version: 1.0.0
 * @Date: 2021/9/15 21:22
 */

package exercise

import (
	"fmt"
)

type record struct {
	get      bool   // 收支
	bill     int    // 收支金额
	balance  int    // 余额
	describe string // 说明
	time     string
}

var money int // 总金额
var records map[int]record
var index int

func StartFamilyAccount() {
	// 初始化
	money = 0
	index = 0
	records = make(map[int]record, 10)
	for {
		fmt.Println("-------家庭收支记账软件-------")
		fmt.Println("  1.收支明细")
		fmt.Println("  2.登记收入")
		fmt.Println("  3.登记支出")
		fmt.Println("  4.退出")
		fmt.Println("---------------------------")
		choose := 0
		fmt.Printf("请选择你要进行的操作：")
		fmt.Scanln(&choose)
		if choose == 4 {
			break
		}
		switch choose {
		case 1:
			queryAll()
		case 2:
			insertGet()
		case 3:
			insertDel()
		}
	}
	fmt.Println("退出家庭收支记账软件...")
}

// 查看收支明细
func queryAll() {
	fmt.Println("收支   收支金额   账户余额   说明")
	// 当前没有收支明细
	if len(records) == 0 {
		fmt.Println("当前还没有收支明细记录~赶快记一笔吧！")
		return
	}
	for _, r := range records {
		getStr := ""
		if r.get {
			getStr = "收入"
		} else {
			getStr = "支出"
		}
		fmt.Printf("%v\t\t%v\t\t%v\t\t%v\n", getStr, r.bill, r.balance, r.describe)
	}
}

// 登记收入
func insertGet() {
	r := record{}
	r.get = true
	fmt.Printf("请输入收入金额：")
	fmt.Scanln(&r.bill)
	fmt.Printf("请输入说明：")
	fmt.Scanln(&r.describe)
	money += r.bill
	r.balance = money
	records[index] = r
	index++
	fmt.Println("登记收入成功！")
}

// 登记支出
func insertDel() {
	r := record{}
	r.get = false
	fmt.Printf("请输入支出金额：")
	fmt.Scanln(&r.bill)
	// 判断当前余额是否足够
	if r.bill > money {
		fmt.Println("当前余额不足，请重新操作！")
		return
	}
	fmt.Printf("请输入说明：")
	fmt.Scanln(&r.describe)
	money -= r.bill
	r.balance = money
	records[index] = r
	index++
	fmt.Println("登记支出成功！")
}
