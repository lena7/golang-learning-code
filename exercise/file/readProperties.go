/**
 * @Author: lena
 * @Description:读取配置文件mysql.properties
 * @Version: 1.0.0
 * @Date: 2021/9/6 11:52
 */

package file

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path"
	"strings"
)

// 反射读取配置文件
type mysql struct {
	username string
	password string
	port     int
	dbname   string
	address  string
}

func ReadMysql() {
	road, _ := os.Getwd()
	road2 := path.Join(road, "exercise/file/mysql.properties")
	file, err := os.Open(road2)
	if err != nil {
		fmt.Println("open err :", err)
		return
	}
	reader := bufio.NewReader(file)
	for {
		s, err := reader.ReadString('\n')
		if err == io.EOF {
			//split := strings.Split(s, "=")
			fmt.Println("read finishes!")
			return
		}
		if err != nil {
			fmt.Println("read err :", err)
			return
		}
		split := strings.Split(s, "=")
		if len(split) == 2 {
			fmt.Println("key:", split[0], "value:", split[1])
		}
	}
	/** 输出结果：
	key: username value: "root"
	key: password value: "root"
	key: port value: 3306
	key: dbname value: "test"
	read finishes!
	*/
}
