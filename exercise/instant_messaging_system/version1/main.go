/**
 * @Author: lena
 * @Description:即时通信系统：构建基础Server
 * @Version: 1.0
 * @Date: 2021/9/20 22:25
 */

package version1

func Main() {
	server := newServer("127.0.0.1", 9000)
	server.start()
}
