/**
 * @Author: lena
 * @Description:服务端
 * @Version: 1.0.0
 * @Date: 2021/9/20 22:26
 */

package version1

import (
	"fmt"
	"net"
)

type server struct {
	ip   string // 服务端ip
	port int    // 服务端端口号
}

// 创建一个服务端
func newServer(ip string, port int) *server {
	return &server{ip, port}
}

// 启动服务器的接口
func (this *server) start() {
	// 服务器监听端口
	listen, err := net.Listen("tcp", fmt.Sprintf("%s:%d", this.ip, this.port))
	if err != nil {
		fmt.Println("[server] listen err :", err)
		return
	}
	defer listen.Close()

	for {
		// 接收客户端连接请求，若没有则一直阻塞在这里
		conn, err := listen.Accept()
		if err != nil {
			fmt.Println("[server] accept err :", err)
			continue
		}
		// 处理接收到的数据
		// 另外开启一个协程来处理，是为了能够让当前协程能快速返回继续接收其他客户端的链接
		go this.handler(conn)
	}
}

// 处理客户端链接
func (this *server) handler(conn net.Conn) {
	// 处理链接
	fmt.Println("获取到链接：", conn)
}
