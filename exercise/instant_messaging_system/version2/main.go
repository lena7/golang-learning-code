/**
 * @Author: lena
 * @Description:即时通信系统：用户上线功能+广播机制
 * @Version: 2.0
 * @Date: 2021/9/21 15:42
 */

package version2

func Main() {
	server := newServer("127.0.0.1", 9000)
	server.start()
}
