/**
 * @Author: lena
 * @Description:客户端：当要操作在线用户列表时，客户端向服务端发出请求，由服务端操作。
 * @Version: 3.0.0
 * @Date: 2021/9/22 14:27
 */

package version3

import (
	"net"
)

type client struct {
	name    string
	address string
	channel chan string // 接收服务端广播消息：客户端取出通道消息进行显示即可
	conn    net.Conn
}

// 创建一个客户端
func newClient(conn net.Conn, serverChannel chan string) *client {
	connAddr := conn.RemoteAddr().String() // 获取客户端链接地址
	client := &client{
		name:    connAddr,
		address: connAddr,
		channel: make(chan string),
		conn:    conn,
	}
	// 创建一个go：负责监听当前客户端的管道是否有需要广播的信息
	go client.listenMessage()
	return client
}

// 监听当前客户端的chennel，一有消息就发送给客户端
func (this *client) listenMessage() {
	// 一直监听管道中的数据
	for {
		// 从管道中取数据，如果没数据就会阻塞直到管道中有数据
		msg := <-this.channel
		// 将信息在当前客户端界面显示
		this.conn.Write([]byte(msg + "\n"))
	}
}
