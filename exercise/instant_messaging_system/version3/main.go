/**
 * @Author: lena
 * @Description:即时通信系统：更名、私聊、下线
 * @Version: 3.0.0
 * @Date: 2021/9/22 14:27
 */
// nc 127.0.0.1 9000

package version3

func Main() {
	server := newServer("127.0.0.1", 9000)
	server.start()
}
