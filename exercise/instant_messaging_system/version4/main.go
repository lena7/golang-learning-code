/**
 * @Author: lena
 * @Description:即时通信系统：封装
 * @Version: 4.0.0
 * @Date: 2021/9/22 16:39
 */

package version4

func Main() {
	server := newServer("127.0.0.1", 9000)
	server.start()
}
