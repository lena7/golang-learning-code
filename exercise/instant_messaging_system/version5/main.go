/**
 * @Author: lena
 * @Description:即时通信系统：超时强踢
 * @Version: 5.0.0
 * @Date: 2021/9/22 18:53
 */
// nc 127.0.0.1 9000

package version5

func Main() {
	server := newServer("127.0.0.1", 9000)
	server.start()
}
