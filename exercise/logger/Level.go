/**
 * @Author: lena
 * @Description:定义日志等级
 * @Version: 1.0.0
 * @Date: 2021/9/7 12:55
 */

package logger

const (
	DEBUG = iota
	TRACE
	INFO
	WARNING
	ERROR
	FATAL
)
