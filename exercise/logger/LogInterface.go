/**
 * @Author: lena
 * @Description:日志应该实现的方法
 * @Version: 1.0.0
 * @Date: 2021/9/7 12:55
 */

package logger

// 定义log接口
type Log interface {
	Debug(msg string)
	TRACE(msg string)
	INFO(msg string)
	WARNING(msg string)
	ERROR(msg string)
	FATAL(msg string)
}
