/**
 * @Author: lena
 * @Description:管理所有学生信息的管理员
 * @Version: 1.0.0
 * @Date: 2021/9/2 17:22
 */

package student_system

type manage struct {
	// 所有学生信息
	students map[int]student
}

// 判断学生是否存在
func (this manage) isExist(id int) bool {
	_, exist := this.students[id]
	return exist
}

// 添加学生
func (this manage) addStudent(s student) {
	this.students[s.id] = s
}

// 删除学生
func (this manage) deleteStudent(id int) bool {
	_, exist := this.students[id]
	// 不存在该学生
	if !exist {
		return false
	}
	delete(this.students, id)
	return true
}

// 查询单个学生信息
func (this manage) getStudent(id int) student {
	return this.students[id]
}

// 查询所有学生
func (this manage) listStudent() map[int]student {
	return this.students
}

// 修改学生信息
func (this manage) updateStudent(s student) {
	this.students[s.id] = s
}
