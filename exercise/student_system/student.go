/**
 * @Author: lena
 * @Description:定义学生结构体
 * @Version: 1.0.0
 * @Date: 2021/9/2 17:22
 */

package student_system

type student struct {
	id   int
	name string
	age  int
}
