/**
 * @Author: lena
 * @Description:学生管理系统
 * @Version: 1.0.0
 * @Date: 2021/9/2 17:22
 */

package student_system

import (
	"fmt"
)

// 学生管理系统:实现查看、添加、修改、删除学生
var m = manage{
	students: map[int]student{},
}

// 系统入口
func Start() {
	for {
		fmt.Println("======欢迎来到学生管理系统======")
		fmt.Println("     1.查看所有学生信息     ")
		fmt.Println("     2.查看某个学生信息     ")
		fmt.Println("     3.添加学生           ")
		fmt.Println("     4.修改学生信息        ")
		fmt.Println("     5.删除学生           ")
		fmt.Println("     6.退出              ")
		fmt.Println("============================")
		fmt.Printf("请输入你要进行的操作：")
		var option int
		fmt.Scanln(&option) // 获取输入值
		switch option {
		case 1:
			getAll()
		case 2:
			getOne()
		case 3:
			addStudent()
		case 4:
			UpdateStudent()
		case 5:
			delStudent()
		case 6:
			return
		default:
			fmt.Println("请输入合法的操作")
		}
	}
}

// 查找所有学生
func getAll() {
	students := m.listStudent()
	for _, s := range students {
		fmt.Println("学号:", s.id, "姓名:", s.name, "年龄:", s.age)
	}
}

// 查询某个学生
func getOne() {
	fmt.Printf("请输入要查询的学生ID：")
	var id int
	fmt.Scanln(&id)
	s := m.getStudent(id)
	fmt.Println("学号:", s.id, "姓名:", s.name, "年龄:", s.age)
}

// 添加学生
func addStudent() {
	var id int
	var name string
	var age int
	fmt.Printf("请输入学生ID：")
	fmt.Scanln(&id)
	res := m.isExist(id)
	if res {
		fmt.Println("该学生已存在！不可重复添加")
		return
	}
	fmt.Printf("请输入学生姓名：")
	fmt.Scanln(&name)
	fmt.Printf("请输入学生年龄：")
	fmt.Scanln(&age)
	s := student{
		id:   id,
		name: name,
		age:  age,
	}
	m.addStudent(s)
	fmt.Println("添加成功！")
}

// 删除学生
func delStudent() {
	var id int
	fmt.Printf("请输入要删除的学生ID：")
	fmt.Scanln(&id)
	res := m.deleteStudent(id)
	if res {
		fmt.Println("删除成功")
	} else {
		fmt.Println("不存在该学生！")
	}
}

// 修改学生信息
func UpdateStudent() {
	var id int
	fmt.Printf("请输入学生ID：")
	fmt.Scanln(&id)
	s := m.getStudent(id)
	fmt.Println("学生当前的信息是=>学号:", s.id, "姓名:", s.name, "年龄:", s.age)
	fmt.Printf("请输入要修改的学生姓名：")
	fmt.Scanln(&s.name)
	fmt.Printf("请输入要修改的学生年龄：")
	fmt.Scanln(&s.age)
	m.updateStudent(s)
	fmt.Println("修改成功！")
}
