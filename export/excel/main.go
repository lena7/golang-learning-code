package main

import (
	"fmt"
	"path"

	"github.com/tealeg/xlsx"
)

var BASE_PATH = "/Users/lena/GoWork/golang/golang-learning-code/export/excel/data/"

func main() {
	read()
}

func read() {
	fileName := path.Join(BASE_PATH, "test.xlsx")
	file, err := xlsx.OpenFile(fileName)
	if err != nil {
		panic(err)
	}
	for _, sheet := range file.Sheet {
		for _, row := range sheet.Rows {
			for _, cell := range row.Cells {
				fmt.Printf("%s ", cell.String())
			}
			fmt.Printf("\n")
		}
	}
}

func generate() {
	// 生成一个新的文件
	file := xlsx.NewFile()
	// 添加sheet页
	sheet, _ := file.AddSheet("Sheet1")
	// 假数据
	titleList := []string{"title1", "title2", "title3"}
	dataList := []string{"1", "2", "3"}
	// 插入表头
	titleRow := sheet.AddRow()
	for _, v := range titleList {
		cell := titleRow.AddCell()
		cell.Value = v
	}
	// 插入内容
	for _, v := range dataList {
		row := sheet.AddRow()
		// row.WriteStruct(v, -1)
		cell := row.AddCell()
		cell.Value = v
	}
	fileName := path.Join(BASE_PATH, "test.xlsx")
	err := file.Save(fileName)
	if err != nil {
		fmt.Println(err)
	}
}
