package main

import (
	"fmt"

	"gopkg.in/ini.v1"
)

func main() {
	cfg, err := ini.Load("/Users/lena/GoWork/golang/golang-learning-code/go-ini/data.conf")
	if err != nil {
		panic(err)
	}
	conf := ReportConfig{}
	err = cfg.Section("report").MapTo(&conf)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", conf)
}
