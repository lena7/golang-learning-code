package main

type Config struct {
	Test   ReportConfig `ini:"test"`
	Report ReportConfig `ini:"report"`
}

type ReportConfig struct {
	Key     string   `ini:"key"`               // 日报类型
	From    string   `ini:"from"`              // 发件人
	To      string   `ini:"to"`                // 收件人
	CC      string   `ini:"cc"`                // 抄送
	Bcc     string   `ini:"bcc"`               // 暗送
	Title   string   `ini:"title"`             // 标题
	Picture []string `ini:"picture" delim:","` // 图片列表
}
