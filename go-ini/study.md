官方api文档：[https://ini.unknwon.cn/docs/howto/work_with_keys](https://ini.unknwon.cn/docs/howto/work_with_keys)

作用：解析ini文件

ini文件格式如下：

```ini
[Section]
Key = Value
# Comment
```

下载go-ini包：
```bash
go get "github.com/go-ini/ini"
```

导包：

```go
import "github.com/go-ini/ini"
```

使用：

```go
func test() {
  cfg,err := ini.Load(文件)
  if err != nil {
        fmt.Printf("Fail to read file: %v", err)
        os.Exit(1)
   }
  // 获取Value
  cfg.Section("填写section").Key("填写key").String()
}
```

获取某一个分区：若分区不存在，则创建一个返回

```go
newSection := cfg.Section("new")
```

获取所有分区：

```go
sections := cfg.Sections()
```

获取所有分区名：

```go
names := cfg.SectionStrings()
```

获取一个分区下所有key集合：如果分区下的key值不是固定的，那我们可以通过该方法获取后直接遍历key，然后将通过key获取值即可。

```go
section := cfg.Section(name)
keys := section.KeyStrings()
```



定义结构体中``的含义：

```bash
comment:描述
ini:对应ini文件的section
delim:分割符
```

结构体映射：MapTo(结构体)
ini文件主要内容如下：
```ini
[Message]
name=lena
age=11
class=计算机183
phone=12345,1024
```
定义结构体：
```go
type Message struct {
	Name string `ini:"name"`
	Aag int `ini:"age"`
	Class string `ini:"class"`
	Phone []string `ini:"phone" delim:","`
}
```
分区映射：映射会根据ini定义的字段名映射到结构体中
```go
m := Message{}
sections := cfg.Section("Message")
err = sections.MapTo(&m)
```

注意：结构体字段开头必须大写，这样外包才能访问。