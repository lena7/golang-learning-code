package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
)

// 转换8进制utf-8字符串到中文
// eg: `\346\200\241` -> 怡
func convertOctonaryUtf8(in string) string {
	s := []byte(in)
	reg := regexp.MustCompile(`\\[0-7]{3}`)

	out := reg.ReplaceAllFunc(s,
		func(b []byte) []byte {
			i, _ := strconv.ParseInt(string(b[1:]), 8, 0)
			return []byte{byte(i)}
		})
	return string(out)
}

func main() {
	input := `\351\241\271\347\233\256\344\270\215\345\255\230\345\234\250` // 原始字符串
	args := os.Args
	if len(args) != 0 {
		input = args[0]
	}
	fmt.Println("转码前：", input)
	// 转化 s2
	output := convertOctonaryUtf8(input)
	fmt.Println("转码后：", output)
}
