/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/14 12:04
 */

package datatype

import (
	"fmt"
	"reflect"
	"testing"
)

func TestArray16(t *testing.T) {
	Array16()
}

func TestArray17(t *testing.T) {
	Array17()
}

func TestTansposeArray(t *testing.T) {
	a := [][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
	fmt.Println(a) // [[1 2 3] [4 5 6] [7 8 9]]
	res := TansposeArray(a)
	expert := [][]int{{1, 4, 7}, {2, 5, 8}, {3, 6, 9}}
	if !reflect.DeepEqual(res, expert) {
		t.Errorf("error")
	}
}

func TestArray15(t *testing.T) {
	Array15()
}

func TestArray19(t *testing.T) {
	Array19()
}
