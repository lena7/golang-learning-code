/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/8/24 14:54
 */

package datatype

import "fmt"

func Const1() {
	// 定义常量 只读
	const a int = 1
	fmt.Println("a =", a) // a = 1
	//a=2	// 若试图更改常量值，会报错
}

func Const2() {
	// 不指明类型
	const b = "hello"
	fmt.Println("b =", b) // b = hello
}

// 全局常量
const c = 3

func Const3() {
	fmt.Println("c =", c) // c = 3
}

func Const4() {
	// 单行定义
	const a, b = "hello", "world"
	fmt.Println(a, b) // hello world
	// 多行定义
	const (
		d = 1
		e = "lena"
	)
	fmt.Println(d, e) // 1 lena
}

const a, b = "hello", "lena"

func Const5() {
	fmt.Println(a, b) // hello lena
}

// 枚举类型
const (
	SPRING = 1
	SUMMER = 2
	AUTUMN = 3
	WINTER = 4
)

func Const6() {
	fmt.Println(SPRING) //1
	fmt.Println(SUMMER) //2
	fmt.Println(AUTUMN) //3
	fmt.Println(WINTER) //4
}
