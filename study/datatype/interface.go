/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/7 17:39
 */

package datatype

import "fmt"

// Interface1 万能类型：interface{}
func Interface1() {
	var a interface{}
	fmt.Printf("%T,%v\n", a, a) // <nil>,<nil>

	a = "hello"
	fmt.Printf("%T,%v\n", a, a) // string,hello

	a = 100
	fmt.Printf("%T,%v\n", a, a) // int,100
}

// 可传入任意类型
func method7(a interface{}) {
	fmt.Printf("%T,%v\n", a, a)
}

func Interface2() {
	a := "hello"
	method7(a) // string,hello
	b := true
	method7(b) // bool,true
}

// 通过方法判断传入的类型再做处理
func method8(a interface{}) {
	// value为a的值，flag为a是否是string类型。若不需要的值，可以用"_"表示
	// 注意：value只有在flag为true的时候，才会是a的值，否则是空
	value, flag := a.(string)
	if flag {
		fmt.Println(value, "is string")
	} else { // else一定要写在if右括号同一行
		fmt.Println(value, "is not string")
	}
}

// 类型断言
func method13(a interface{}) {
	switch t := a.(type) {
	case string:
		fmt.Println(t, "是string型")
	case int:
		fmt.Println(t, "是int型")
	case bool:
		fmt.Println(t, "是bool型")
	}
}

func Interface4() {
	method13("123") // 123 是string型
	method13(123)   // 123 是int型
	method13(true)  // true 是bool型
}

func Interface3() {
	a := 100
	method8(a) //  is not string
	b := "hello"
	method8(b) // hello is string
}

func Interface5(a interface{}) string {
	s := ""
	switch t := a.(type) {
	case string:
		s = fmt.Sprintf("%s", t)
	case int:
		s = fmt.Sprintf("%d", t)
	}
	return s
}

// 强制类型转换
func turnType() {
	var a interface{}
	a = 10
	//var b int
	//b=a.(int)				// 没有报错
	fmt.Println(a.(int)) // 10
}

// 定义接口
type runner interface {
	run()
}

type jumper interface {
	jump()
}

// 接口内可包含接口 直接传入内嵌接口的名称即可
type mover interface {
	runner
	jumper
}
