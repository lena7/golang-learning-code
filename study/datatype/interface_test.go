package datatype

import (
	"fmt"
	"reflect"
	"testing"
)

func TestInterface5(t *testing.T) {
	r1 := Interface5("23_t")
	if !reflect.DeepEqual(r1, "23_t") {
		fmt.Println("err1")
	}
	r2 := Interface5(2)
	if !reflect.DeepEqual(r2, "2") {
		fmt.Println("err2")
	}
}
