/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/8/24 15:30
 */

package datatype

import "fmt"

func Iota1() {
	// iota初始值是0
	const a = iota
	fmt.Println(a) // 0
	//var a=iota //报错：iota只能配合const
}

func Iota2() {
	// iota每次默认自增1
	const (
		a = iota
		b
		c
		d
	)
	fmt.Println(a, b, c, d) // 0 1 2 3
}

func Iota3() {
	// 不同const的iota互不干扰
	const (
		a = iota
		b
	)
	fmt.Println(a, b) // 0 1
	const (
		c = iota
		d
	)
	fmt.Println(c, d) // 0 1
}

func Iota4() {
	// 同行iota的值相等 iota=0
	const a, b = iota + 1, iota + 10
	fmt.Println(a, b) // 1 10
	const (
		// iota=0,c=1,d=10
		c, d = iota + 1, iota + 10
		// iota=1,e=3,f=11
		e, f = iota + 2, iota + 10
	)
	fmt.Println(c, d) // 1 10
	fmt.Println(e, f) // 3 11
}

func Iota5() {
	// iota规则延续第一个定义的规则
	// 如果定义时一行定义了两个常量，接下来都要遵循这个规则，不能只定义一个
	const (
		// iota=0,g=0,c=0
		g, h = iota * 2, iota * 4
		// iota=1,i=1*2=2,j=1*4=4
		i, j
		// iota=2,l=2*2=4,m=2*4=8
		l, m
		//n // 只定义一个会报错
	)
	fmt.Println(g, h) // 0 0
	fmt.Println(i, j) // 2 4
	fmt.Println(l, m) // 4 8
}

func Iota6() {
	const (
		a = iota + 5 // 5:0+5
		k            // 6:1+5
		b = 10       // 定义了常量会打断iota规则 后续的值如果没使用iota都是此常量
		c            // 10:跟着b的值
		d = iota     // 4:iota的规则已经被打破 当前iota与第一个相差4行 iota=4
		g            // 5:iota+1
	)
	fmt.Println(a, k, b, c, d, g) // 5 6 10 10 4 5

	const (
		e = 1
		f
	)
	fmt.Println(e, f) // 1 1
}

func Iota7() {
	const (
		a = iota * 2
		b
		c
	)
	fmt.Println(a, b, c)
}
