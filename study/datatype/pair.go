/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/8/26 15:30
 */

package datatype

import (
	"fmt"
	"io"
	"os"
)

func Pair1() {
	a := "hello"
	var b interface{}
	fmt.Printf("%T\n", b) // <nil>
	b = a
	fmt.Printf("%T\n", b) // string
}

func Pair2() {
	// tty:pair<type:*os.File,value:"/lena"文件描述符>;error为异常
	tty, error := os.OpenFile("/dev/tty", os.O_RDWR, 0)
	if error != nil {
		fmt.Println(tty, error)
	}
}

// 在参数传递的时候pair是永远不变的,也跟着传递
func Pair3() {
	// tty:pair<type:*os.File,value:"/lena"文件描述符>;error为异常
	tty, error := os.OpenFile("/dev/tty", os.O_RDWR, 0)
	if error != nil {
		fmt.Println(tty, error)
	}
	// 定义一个type=io.Reader(是interface) 此时type和value都未知
	var reader io.Reader
	// reader:pair<type:*os.File,value:"/lena"文件描述符>
	reader = tty
	fmt.Println(reader)
}
