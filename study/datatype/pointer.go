/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/8/24 15:30
 */

package datatype

import (
	"fmt"
	"os"
)

// 使用 &变量 : 获取的是变量的地址
func Pointer1() {
	a := 1
	fmt.Println(&a) // 打印a的地址：0xc00000a0a8
}

// 使用 *变量 : 获取的是变量的值
func Method(p *int) {
	fmt.Println(p, *p, &p) // 0xc000126058 2 0xc00014a018
	// *p获取的是传入地址对应的值，修改对应的值为10
	*p = 10
}

func Pointer2() {
	a := 2
	//Method(a)	// 报错：不能从int->*int
	Method(&a)            // 传入a的地址
	fmt.Println("a =", a) // a = 10
}

func Pointer3() {
	// 未初始化,值为0
	var a *int
	// 地址值为0的变量=nil
	fmt.Println(a) // <nil>
	// 必须要先分配空间 才能赋值
	//*a=100	// error: invalid memory address or nil pointer dereference
	a = new(int)
	*a = 100
	// *取值,&取地址,变量值为地址值
	fmt.Println(a, *a, &a) // 0xc00000a0f0 100 0xc000006028
}

// 判断是否是某一指针类型,要指明指针类型
func judgeType(a interface{}) {
	pointer := a.(*os.File)
	fmt.Println(pointer) // <nil>
}
func Pointer4() {
	open, _ := os.Open("./text.txt")
	judgeType(open)
}
