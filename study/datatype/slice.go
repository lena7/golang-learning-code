package datatype

import (
	"fmt"
	"sort"
)

func SliceRemove() {
	str := []string{"lena", "csf", "nihao", "nihao1", "abs", "", ""}
	sort.Strings(str)
	index := 0
	for _, n := range str {
		if n == "" {
			index++
		} else {
			break
		}
	}
	if index != 0 {
		str = str[index:]
	}
	fmt.Printf("%+v\n", str)
}
