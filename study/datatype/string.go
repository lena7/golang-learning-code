package datatype

import (
	"fmt"
)

func StringType1(str string) {
	if len(str) > 0 {
		fmt.Printf("input:%s len = %d\n", str, len(str))
		return
	}
	fmt.Printf("input:%s len = 0\n", str)
}
