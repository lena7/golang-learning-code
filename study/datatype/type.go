/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/2 15:30
 */

package datatype

import (
	"fmt"
)

/**
bool 布尔值 true/false
uint 无符号整数
	uint8:无符号8位整数 [0,255]
	uint16:无符号16位整数 [0,65535]
	uint32:无符号32位整数 [0,4294967295]
	uint64:无符号16位整数 [0,184467440737095]
int 有符号整数 32位os就是int32 64位os就是int64
	int8:有符号8位整数 [-128,127]
	int16:有符号16位整数 [-32768,32767]
	int32:有符号32位整数 [-2147483648,2147483637]
	int64:有符号16位整数 [-9223372036854775808，9223372036854775807]
float 浮点数
	float32
	float34
complex
	complex64:32位实数和虚数
	complex128:64位实数和虚数
byte =unit8 代表了 ASCII 码的一个字符
rune =int32 代表一个 UTF-8 字符，当需要处理中文、日文或者其他复合字符时，则需要用到 rune 类型。
uintptr 无符号整型,用于存放指针
*/

// type重新定义(取别名)的数据类型 能够进行强制转换
type i int

func Type1() {
	var a int
	a = 10
	var b i
	b = i(a)
	fmt.Printf("a type = %T\n", a) // a type = int
	fmt.Printf("b type = %T\n", b) // b type = datatype.i
}

// 具有相同属性名称、类型的结构体也能够强制转换
type stu struct {
	sid  int
	name string
}

func Type2() {
	a := student{1001, "lena"}
	var b stu
	b = stu(a)
	fmt.Printf("a type = %T\n", a) // a type = datatype.student
	fmt.Printf("b type = %T\n", b) // b type = datatype.stu
}

// 实现了String()方法的变量，在fmt.Println()会调用该方法??为什么不行
func (this stu) String() {
	fmt.Println("我是自定义String方法：", this.sid, this.name)
}

func String1() {
	b := stu{1001, "lena"}
	fmt.Println(b)
}

type Opts struct {
	data []string
}

func WithData(data []string) TestFun {
	return func(o *Opts) {
		o.data = data
	}
}

type TestFun func(o *Opts) // 入参o会注入到Opts对象中

func Type3(opts ...TestFun) {
	opt := &Opts{}
	for _, o := range opts {
		o(opt)
	}
	fmt.Printf("opt:%+v\n", opt)
}
