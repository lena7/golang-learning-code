package datatype

import (
	"fmt"
	"strconv"
)

// Uint1 uint32 -> string
func Uint1() {
	var ttt uint32
	ttt = 10032
	str := string(strconv.Itoa(int(ttt)))
	fmt.Println(str)
}

// Uint11 uint32 -> string
func Uint11() {
	var ttt uint32
	ttt = 10032
	str := fmt.Sprintf("%d", ttt)
	fmt.Println(str)
}

// Uint3 string -> uint32
func Uint3() {
	var t uint32
	str := "12"
	res, _ := strconv.Atoi(str)
	t = uint32(res)
	fmt.Println(t)
}

// Uint2 uint32 -> int
func Uint2() {
	var t uint32
	var res int
	t = 12
	res = int(t)
	fmt.Println(res)
}
