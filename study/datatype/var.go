/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/8/24 15:30
 */

package datatype

import "fmt"

// 定义变量
// 方式一：初始化 但不赋值
func Var1() {
	var a int
	// int型初始化默认是0
	fmt.Println("a = ", a) // a =  0
}

// 方式二：初始化并赋值
func Var2() {
	var b int = 10
	fmt.Println("b = ", b) // b =  10
}

// 方式三：初始化时省略数据类型（自动匹配）
func Var3() {
	var c = 10
	fmt.Println("c = ", c) // c =  10
}

// 方式四：利用“:=”符号
func Var4() {
	d := 10
	fmt.Println("d = ", d) // d =  10
}

// 多行的变量声明
// 局部变量
func Var5() {
	var (
		a int = 100
		b int = 10
	)
	fmt.Println("a =", a, "b =", b) // a = 100 b = 10
}

// 全局变量
var (
	ga int = 20
	gb int = 10
)

func Var6() {
	fmt.Println("ga =", ga, "gb =", gb) // ga = 20 gb = 10
}

// 单行定义多变量
// 局部变量
func Var7() {
	var a, b = 10, 20
	fmt.Println("a =", a, "b =", b) // a = 10 b = 20
	var c, d = true, "hello"
	fmt.Println("c =", c, "d =", d) // c = true d = hello
}

// 全局变量
var gc, gd = "a", "b"

func Var8() {
	fmt.Println("gc =", gc, "gd =", gd) // gc = a gd = b
}

// 变量作用域:函数内部>外面>全局
func Var9() {
	gc := "outside"
	// 匿名函数
	func() {
		gc := "inside"
		fmt.Println(gc) // inside
	}()
	fmt.Println(gc) // outside
}
