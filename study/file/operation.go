/**
 * @Author: lena
 * @Description:文件基本操作
 * @Version: 1.0.0
 * @Date: 2021/9/15 21:44
 */

package file

import (
	"fmt"
	"os"
)

// 判断文件或文件夹是否存在：os.Stat()
func File1(name string) {
	info, err := os.Stat(name)
	if err != nil {
		fmt.Println("err :", err)
		return
	}
	fmt.Println("文件名是：", info.Name())
	fmt.Println("文件大小：", info.Size())
	fmt.Println("是否是文件夹：", info.IsDir())
}
