/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/15 21:47
 */

package file

import (
	"fmt"
	"os"
	"testing"
)

func TestFile1(t *testing.T) {
	fmt.Println(os.Getwd()) // E:\golang\GoWorks\helloworld\study\file <nil>
	s := []string{"read.go", "operation.go", "hhhh.txt", "../lib/atomic.go"}
	for _, s1 := range s {
		File1(s1)
		fmt.Println("---------")
	}
}
