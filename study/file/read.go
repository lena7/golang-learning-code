/**
 * @Author: lena
 * @Description:读取文件中的信息
 * @Version: 1.0.0
 * @Date: 2021/9/6 13:42
 */

package file

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

// 读文件

/**
Read1 按字节读取
使用Read方法读取：func (f *File) Read(b []byte) (n int, err error).
注意：一次Read读取出的字节数与[]byte的大小有关
*/

func Read1() {
	// 若使用相对路径,可以使用os.Getwd()获取当前路径
	file, err := os.Open("./exercise/file/read.go") // (*File, error)
	// 打开文件过程中是否报错
	if err != nil { // 如果没有报错,则err为<nil>
		fmt.Println("open file failed! err:", err)
		return
	}
	// 结束的时候要关闭文件
	defer file.Close()
	// 要传入一个[]byte类型参数,第二个参数决定每次读取多少字节
	b := make([]byte, 128)
	// 循环读取,直到读完return
	for {
		// 按字节读取:其中n是读取的字节个数,读取出的字节数还与[]byte的大小有关
		n, err := file.Read(b)
		// 读文件过程中是否出错
		if err != nil { // 如果没有报错,则err为<nil>
			fmt.Println("read file failed! err:", err)
			return
		}
		//fmt.Println("read date",n,"byte success")
		// 若没有string转换 读取出来的是数字
		fmt.Println(string(b[:n]))
		// 读取字节小于byte大小,说明读完了 || 当文件读完的时候会返回io.EOF
		if n < 128 || err == io.EOF {
			return
		}
	}
}

// Read2 bufio按行读取
func Read2() {
	// 打开文件
	file, err := os.Open("./exercise/file/read.go") // (*File, error)
	if err != nil {                                 // 如果没有报错,则err为<nil>
		fmt.Println("open err :", err)
		return
	}
	defer file.Close()
	// 读取文件
	// func NewReader(rd io.Reader) *Reader
	reader := bufio.NewReader(file)
	for {
		// reader读取到delim停止 func (b *Reader) ReadString(delim byte) (string, error)
		readString, err := reader.ReadString('\n')
		if err == io.EOF {
			// 文件读完了但是还有数据没有输出
			if len(readString) != 0 {
				fmt.Println(readString)
			}
			return
		}
		if err != nil {
			fmt.Println("read err :", err)
			return
		}
		fmt.Println(readString)
	}
}

// Read3 ioutil一次性读取整个文件
func Read3() {
	file, err := ioutil.ReadFile("./exercise/file/read.go")
	if err != nil {
		fmt.Println("read err :", err)
		return
	}
	fmt.Println(string(file))
}

// *统计文件中各种字符的个数：字母、数字、其他
