/**
 * @Author: lena
 * @Description:向文件中写信息
 * @Version: 1.0.0
 * @Date: 2021/9/6 13:42
 */

package file

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
)

// 写文件

// Write和WriteString
func Write1() {
	// 若无文件则创建 打开文件并清空
	file, err := os.OpenFile("./exercise/file/test.txt", os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		fmt.Println("open err :", err)
		return
	}
	defer file.Close()
	// 写入字节：func (f *File) Write(b []byte) (n int, err error)
	file.Write([]byte("hello lena\n"))
	// 写入字符串： func (f *File) WriteString(s string) (n int, err error)
	file.WriteString("hello lena")
}

// Write2 bufio.NewWriter
func Write2() {
	file, err := os.OpenFile("./exercise/file/test.txt", os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		fmt.Println("open err :", err)
		return
	}
	defer file.Close()
	// func NewWriter(w io.Writer) *Writer
	writer := bufio.NewWriter(file)
	writer.WriteString("先将要写入的内容写入缓存中,在执行flush的时候才会被写到文件中")
	// 将缓存的数据写入文件中
	writer.Flush()
}

// Write3 ioutil.WriteFile
func Write3() {
	// 若文件不存在 会创建文件；会清空文件原先内容
	err := ioutil.WriteFile("./exercise/file/test.txt", []byte("hello world"), 0666)
	if err != nil {
		fmt.Println("write err :", err)
	}
}

// Write4 向文件中间插入内容:通过seek方法能够移动文件中的光标
func Write4() {
	// 打开文件
	// 移动文件光标到要插入内容的位置
	// 写入
}
