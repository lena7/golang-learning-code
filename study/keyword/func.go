/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/1 19:42
 */

package keyword

import (
	"fmt"
	"strings"
)

// 一个参数
func Method1(a int) { // 传入2
	fmt.Println("a =", a) // a = 2
	a = 10
	fmt.Println("a =", a) // a = 10
}

// 多个参数
func Method2(a int, b string, c bool) { // 传入 10,"b",true
	fmt.Println("a =", a) // a = 10
	fmt.Println("b =", b) // b = b
	fmt.Println("c =", c) // c = true
}

// 有一个返回值
func Method3() int {
	return 3
}

// 有多个返回值
func Method4() (int, int) {
	return 4, 5
}

// 多个参数多个返回值
func Method5(e int, f int) (int, int) {
	return e, f
}

// 多个返回值赋变量名称
func Method6(a int, b int) (c int, d int) {
	// 不用定义 因为作为返回值变量已经初始化
	c = a + 10
	d = b + 10
	return
}

// 函数作为参数: 形参名 func(参数类型列表) 返回值类型
func method11(f func(int, int) int, a int, b int) int {
	return f(a, b)
}

func add(x, y int) int {
	return x + y
}

func Func1() {
	// 以方法作为参数的时候无需传入类型,但需要与定义的方法参数类型数量一致
	res := method11(add, 1, 2)
	fmt.Println(res) // 3
}

// 匿名函数:仅对函数存在的作用域内有效
func Func2() {
	f1 := func() {
		fmt.Println("匿名函数1")
	}
	// 可执行多次
	f1()
	f1()

	// 自调用：只能执行一次
	func() {
		fmt.Println("匿名函数2")
	}()
}

// 闭包
func addSuffix(suffix string) func(string) string {
	return func(name string) string {
		if !strings.HasSuffix(name, suffix) {
			return name + suffix
		}
		return name
	}
}

func Func3() {
	jpg := addSuffix(".jpg")
	png := addSuffix(".png")
	res1 := jpg("hello")
	res2 := png("world")
	fmt.Println(res1) // hello.jpg
	fmt.Println(res2) // world.png
}
