/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/8/24 13:42
 */

package keyword

/**

// 导一个包
import "fmt"

// 导多个包 方式一
import(
	"fmt"
	"time"
)

// 导多个包 方式二

import "fmt"
import "time"

*/

/** 在包名前面使用_

import (
	_ "helloworld/test1"
)

func main() {
	// 方法中即使没有用到导入包，也不会报错
}

*/

/*	定义别名
import lena "helloworld/study"

func main() {
	//study.Method4()	// 原先的这种调用方法会报错
	lena.Goto1()	// 利用别名调用包的方法
}*/

/** 在导包前面加入.

// 原先是这样导入包
import "helloworld/test1"

func main() {
	// 通过包名.方法名调用其他包的方法
	test1.Method4()
}

// 在导包前面加入.
import . "helloworld/test1"

func main() {
	//test1.Method4()	// 原先的这种调用方法会报错
	// 省略包名，可以直接调用其他包下的方法。
	Method4()
}

*/
