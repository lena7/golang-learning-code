/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/1 17:39
 */

package keyword

import "fmt"

// make可用于slice、map、chan的内存分配
func Make1() {
	a := make([]int, 2)
	b := make(map[int]string, 2)
	c := make(chan int)
	fmt.Println(a) // [0 0]
	fmt.Println(b) // map[]
	fmt.Println(c) // 0xc00000e180
}

/**
make和new的区别
1.make和new都用来申请空间
2.new很少用,一般用于给基本数据类型申请内存.而make用于slice、map、chan
3.make函数返回的是分配内存空间所对应类型的本身
4.new只要用来分配值类型，返回的都是指针类型
*/

func Make2() {
	a := new(*string)
	fmt.Printf("%T\n", a)  // **string
	fmt.Println(a, &a, *a) // 0xc00014c020 0xc00014c018 <nil>
	//b:=make(*string)	//error
}
