/**
 * @Author: lena
 * @Description:原子类
 * @Version: 1.0.0
 * @Date: 2021/9/9 21:23
 */

package lib

import (
	"fmt"
	"sync/atomic"
)

// 原子操作 Load读、Store写、Add增加值、Swap交换、CompareAndSwap比较并交换
type atomicInt struct {
	val int64
}

func Atomic1() {
	a := atomicInt{
		val: 0,
	}
	for i := 0; i < 100; i++ {
		a.add()
	}
	fmt.Println(a.val) // 输出：100
}
func (this *atomicInt) add() {
	// func AddInt64(addr *int64, delta int64) (new int64)
	// 传递的是指针是因为，如果传递的是基本数据类型，那么是一个副本，修改无效
	atomic.AddInt64(&this.val, 1)
}
