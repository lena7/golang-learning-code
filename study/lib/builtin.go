/**
 * @Author: lena
 * @Description:builtin包定义了一些通用操作
 * @Version: 1.0.0
 * @Date: 2021/9/14 13:29
 */

package lib

import "fmt"

// len(string) : 计算字符串长度
func Len(s string) int {
	// 编码为utf-8：ascii字符占1个字节，汉字占3个字节
	return len(s)
}

// 字符串遍历中文问题 : 转为[]rune切片
func Rune() {
	s := "仲恺农业工程学院"
	fmt.Println(s) // 仲恺农业工程学院
	for i := 0; i < len(s); i++ {
		fmt.Printf("%c ", s[i]) // ä » ² æ  º å   ä ¸  å · ¥ ç ¨  å ­ ¦ é  ¢
	}
	fmt.Println("===========")
	r := []rune(s)
	for i := 0; i < len(r); i++ {
		fmt.Printf("%c ", r[i]) // 仲 恺 农 业 工 程 学 院
	}
}
