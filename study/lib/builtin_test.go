/**
 * @Author: lena
 * @Description:builtin测试类
 * @Version: 1.0.0
 * @Date: 2021/9/14 13:32
 */

package lib

import (
	"reflect"
	"testing"
)

func TestLen1(t *testing.T) {
	res := Len("hello")
	if !reflect.DeepEqual(res, 5) {
		t.Errorf("error")
	}
	res = Len("你好")
	if !reflect.DeepEqual(res, 6) {
		t.Errorf("error")
	}
	// 英文逗号
	res = Len("你好,world")
	if !reflect.DeepEqual(res, 12) {
		t.Errorf("error")
	}
	// 中文逗号
	res = Len("你好，world")
	if !reflect.DeepEqual(res, 14) {
		t.Errorf("error")
	}
}
