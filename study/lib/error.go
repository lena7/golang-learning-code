/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/14 17:18
 */

package lib

import (
	"errors"
	"fmt"
)

// 自定义异常1
func createError() error {
	fmt.Println("coming")
	err := errors.New("创建异常！")
	fmt.Println("over")
	return err
}
func Errors1() {
	err := createError()
	if err != nil {
		fmt.Println("error :", err)
	}
}

// Errors2 自定义异常2
func Errors2() {
	panic("我错了！") // 报错：panic: 我错了！
}

// 出现异常：不会执行异常后的程序
func byzero() {
	n1 := 10
	n2 := 0
	n3 := n1 / n2 // panic: runtime error: integer divide by zero
	fmt.Println(n3)
}
func HasError() {
	fmt.Println("coming!")
	byzero() // 在这之前出现异常，因此后面的都不会执行
	fmt.Println("over!")
}

// byzero2 捕获异常：recover()，捕获后异常下面的程序会继续执行
func byzero2() {
	// 可能出现异常，在该方法结尾捕获异常
	defer func() {
		err := recover()
		if err != nil {
			fmt.Println("err :", err)
		}
	}()
	n1 := 10
	n2 := 0
	n3 := n1 / n2   // 此处的异常没有向上提交，而是被recover捕获
	fmt.Println(n3) // 当前方法内异常还没有被捕获，因此异常后没有继续执行。但方法结束后异常就被捕获了，因此调用该方法的方法还能继续向下执行。
}

func CatchError() {
	fmt.Println("comming") // comming
	byzero2()              // err : runtime error: integer divide by zero
	fmt.Println("over!!")  // over!!
}
