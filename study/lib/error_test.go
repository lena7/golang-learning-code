package lib

import (
	"fmt"
	"testing"
)

func TestCatchError(t *testing.T) {
	CatchError()
}

func TestErrors1(t *testing.T) {
	Errors1()
}

func TestErrors(t *testing.T) {
	// 可能出现异常，在该方法结尾捕获异常
	defer func() {
		err := recover()
		if err != nil {
			e := fmt.Sprintf("%s", err)
			fmt.Println(e)
		}
	}()
	n1 := 10
	n2 := 0
	n3 := n1 / n2   // 此处的异常没有向上提交，而是被recover捕获
	fmt.Println(n3) // 当前方法内异常还没有被捕获，因此异常后没有继续执行。但方法结束后异常就被捕获了，因此调用该方法的方法还能继续向下执行。
}
