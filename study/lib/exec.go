package lib

import (
	"fmt"
	"io/ioutil"
	"os/exec"
)

// Exec1 执行命令并获取输出结果
func Exec1() {
	cmd := exec.Command("/bin/bash", "-c", "ls -alh")
	stdout, _ := cmd.StdoutPipe() // 创建输出信息的管道
	defer stdout.Close()
	if err := cmd.Start(); err != nil {
		fmt.Println(err)
	}
	fmt.Println("执行的命令是：", cmd)
	res, _ := ioutil.ReadAll(stdout)
	fmt.Println("返回结果", string(res))
}

// Exec2 拼接命令
func Exec2(name string) {
	cmd := exec.Command("/bin/bash", "-c",
		fmt.Sprintf("ls -alh|grep %s", name))
	stdout, _ := cmd.StdoutPipe() // 创建输出信息的管道
	defer stdout.Close()
	if err := cmd.Start(); err != nil {
		fmt.Println(err)
	}
	fmt.Println("执行的命令是：", cmd)
	res, _ := ioutil.ReadAll(stdout)
	fmt.Println("返回结果", string(res))
}
