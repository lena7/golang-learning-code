/**
 * @Author: lena
 * @Description:flag包用于解析命令行参数
 * @Version: 1.0.0
 * @Date: 2021/9/12 17:21
 */

package lib

import (
	"flag"
	"fmt"
)

// Flag1 添加命令行参数 : 注意返回的是指针类型
func Flag1() {
	// flag.类型(参数名,默认值,输入提示 --help会提示) *类型
	name := flag.String("name", "default", "tip")
	/**
	解析参数：输入格式可以为以下几种
	1.-name xxx
	2.--name xxx
	3.-name=xxx
	4.--name=xxx
	*/
	flag.Parse()
	fmt.Println(*name)
}

// Flag2 命令行参数信息
func Flag2() {
	flag.Parse()
	fmt.Println(flag.Args())  // 命令行后的其他参数,return => []string
	fmt.Println(flag.NArg())  // 命令行参数后的其他参数个数
	fmt.Println(flag.NFlag()) // 命令行参数个数
}
