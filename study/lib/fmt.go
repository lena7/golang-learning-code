/**
 * @Author: lena
 * @Description:fmt
 * @Version: 1.0.0
 * @Date: 2021/9/1 19:42
 */

package lib

import (
	"bufio"
	"fmt"
	"helloworld/tools/typex"
	"os"
)

/**
fmt.printf()输出格式
- %v：输出值
- %+v：输出值,若为结构体输出时会添加字段名
- %#v：该值对应的Go语法表示形式
- %T：值的类型
- %%：输出百分号
- 进制%+d=十进制;b=二进制;o=八进制;x=十六进制;
- 特定类型：%c=字符,%s=字符串,%p=指针;%f=浮点数
*/

func Fmt1() {
	var s struct {
		id   int
		name string
	}
	s.id = 1001
	s.name = "lena"
	fmt.Printf("%v\n", s)  // {1001 lena}
	fmt.Printf("%+v\n", s) // {id:1001 name:lena}
}

// Fmt2 获取输入值
func Fmt2() {
	fmt.Printf("请输入数字")
	var id int
	fmt.Scanln(&id)
	fmt.Println("你输入的是：", id)
}

// Fmt3 使用fmt.Scanln获取输入值遇到空格或\n会停止
func Fmt3() {
	fmt.Printf("请输入：")
	var s string
	fmt.Scanln(&s)           // a b c
	fmt.Println("你输入的是：", s) // a
}

// Fmt4 使用os.Stdin读取遇到空格不会停止
func Fmt4() {
	fmt.Printf("请输入：")
	reader := bufio.NewReader(os.Stdin) // a b c
	s2, _ := reader.ReadString('\n')
	fmt.Println(s2) // a b c
}

// Fmt5 格式化字符串并返回:Sprintf
func Fmt5() {
	i := []int{1, 2, 3, 4, 5}
	s := fmt.Sprintf("%v\thello", i)
	fmt.Println(s) // [1 2 3 4 5]	hello

	s1 := "hello"
	id := 1001
	str := fmt.Sprintf("%s,%d\n", s1, id)
	fmt.Println(str)      // hello,1001
	fmt.Printf("%T", str) // string
}

// Fmt6 数组格式化拼接方式
func Fmt6() {
	a := []int{1, 2, 3, 4, 5}
	str := fmt.Sprintf("%d", a)
	fmt.Println(str) // [1 2 3 4 5]
	b := []string{"abc", "def", "lena"}
	strb := fmt.Sprintf("%s", b)
	fmt.Println(strb) // [abc def lena]
}

// Fmt7 float64的拼接方式
func Fmt7() {
	var a float64
	a = 79.392
	str := fmt.Sprintf("%f", a) // 79.392000
	fmt.Println(str)
	str2 := fmt.Sprintf("%s", typex.FloatToStr(a))
	fmt.Println(str2) // 79.392
}
