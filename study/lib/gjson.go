package lib

// 导入依赖：go get "github.com/tidwall/gjson"

import (
	"fmt"
	"github.com/tidwall/gjson"
)

// GJson1 获取json字符串中的某个字段 返回的是Result对象，获取对象中的Str字段表示内容
func GJson1() {
	str := "{\n  \"mal_loss_of_income\": 0,\n  \"mal_resp_group_name\": \"\",\n  \"mal_emerg_level\": null,\n  \"code\": 0,\n  \"mal_resp_group_id\": \"d0909\",\n  \"mal_last_modify\": \"2021-10-20 17:00:15\",\n  \"mal_effect_product\": \"\",\n  \"mal_meet_time\": \"2021-09-28 11:12:00\"}"
	fmt.Println(gjson.Get(str, "mal_resp_group_id"))
	fmt.Println(gjson.Get(str, "mal_last_modify").Str)
	// 获取一个不存在的值
	fmt.Println(gjson.Get(str, "nihao").Exists())
}

// GJson2 能否成功获取数组数据
func GJson2() {
	str := "[{\"id\":1,\"name\":\"nihao\"},{\"id\":2,\"name\":\"hello\"]"
	fmt.Println(gjson.Get(str,"0.name"))
}