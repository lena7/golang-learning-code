/**
 * @Author: lena
 * @Description:json结构体属性开头必须大写才能够被序列化
 * @Version: 1.0.0
 * @Date: 2021/8/27 19:42
 */

package lib

import (
	"encoding/json"
	"fmt"
)

type Grade struct {
	Sid   int `json:"student_id"`
	Cid   int `json:"class_id"`
	Grade int `json:"grade"`
}

func Json1() {
	grade := Grade{1001, 183, 99}
	// json编码：将结构体转换成json格式 其中变量名采用结构体标签定义的json值
	jsonStr, err := json.Marshal(grade)
	if err != nil { // 无异常的时候err为<nil>
		fmt.Println(err)
	}
	fmt.Printf("%s\n", jsonStr) // {"student_id":1001,"class_id":183,"grade":99}
	// json解码：将json格式的数据转换为结构体对象
	grade1 := Grade{}
	// func Unmarshal(data []byte, v interface{}) error
	err = json.Unmarshal(jsonStr, &grade1)
	if err != nil { // 无异常的时候err为<nil>
		fmt.Println(err)
	}
	fmt.Println(grade1) // {1001 183 99}
}

// RateOfFunction 执行runby脚本返回的的json结构
type RateOfFunction struct {
	BizName      string            `json:"biz_name"`
	FunctionInfo map[string]string `json:"function_info"`
}

func Json2() {
	str := "{\"function_info\": {\"核心接口\":\"--\"},\"biz_name\": \"服务台\"}"
	// str:="{\"function_info\": {\"核心接口\":1.0},\"biz_name\": \"服务台\"}"
	g := RateOfFunction{}
	err := json.Unmarshal([]byte(str), &g)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(g)
}

// Json3 测试json能否封装数组 可以
func Json3() {
	str := "[{\"id\":1,\"name\":\"nihao\",\"age\":12},{\"id\":2,\"name\":\"hello\"}]"
	type s struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
		Age  int    `json:"age"`
	}
	res := []s{}
	json.Unmarshal([]byte(str), &res)
	fmt.Println(res)
}

// Json4 json将结构体转换成字符串的时候，符号 < > & 会转码，但是当换回结构体的时候，就会正常显示。
func Json4() {
	type s struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
		Age  int    `json:"age"`
	}
	res := []s{{1, "<你好>", 12}, {2, "wo&lena", 12}}
	resstr, _ := json.Marshal(res)
	fmt.Println("string：", string(resstr)) // [{"id":1,"name":"\u003c你好\u003e","age":12},{"id":2,"name":"wo\u0026lena","age":12}]
	sts := []s{}
	json.Unmarshal(resstr, &sts)
	fmt.Println(sts) // [{1 <你好> 12} {2 wo&lena 12}]
}

// Json5 封装json部分数组的数据
func Json5() {
	type s struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
	}
	sts := []s{}
	str := "[{\"id\":1,\"name\":\"\\u003c你好\\u003e\",\"age\":12},{\"id\":2,\"name\":\"wo\\u0026lena\",\"age\":12}]"
	json.Unmarshal([]byte(str), &sts)
	fmt.Println(sts) // [{1 <你好>} {2 wo&lena}]
}

// Json6 查看结构体存在的字段 但string没有的怎么转换
func Json6() {
	type s struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
		Age  int    `json:"age"`
	}
	sts := []s{}
	str := "[{\"id\":1,\"name\":\"\\u003c你好\\u003e\"},{\"id\":2,\"name\":\"wo\\u0026lena\"}]"
	json.Unmarshal([]byte(str), &sts)
	fmt.Println(sts) // [{1 <你好> 0} {2 wo&lena 0}]
}

// Json7 空数组json转换显示null 而不是[] 分配空间后显示[]
func Json7() {
	type requestData struct {
		T          string
		Data       string
		EffectData []string
		ReplayData []string
	}
	ts := requestData{T: "nihao", EffectData: make([]string, 0)}
	str, _ := json.Marshal(ts)
	fmt.Println(string(str)) // {"T":"nihao","Data":"","EffectData":[],"ReplayData":null}
}

// Json8 转义
func Json8() {
	str := "[{\"open\": \"true\",\"pId\": 0,\"id\": \"cls_1\",\"name\": \"\\u793e\\u4ea4\\u4e1a\\u52a1\"},{\"open\": \"true\",\"pId\": 0,\"id\": \"cls_11\",\"name\": \"\\u91d1\\u878d\\u4e1a\\u52a1\"}]"
	type Str struct {
		Open string `json:"open"`
		PId  int    `json:"pId"`
		Id   string `json:"id"`
		Name string `json:"name"`
	}
	res := []Str{}
	err := json.Unmarshal([]byte(str), &res) // 第二个参数如果不添加&符号会报错：json: Unmarshal(non-pointer []lib.Str)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(res) // [{true 0 cls_1 社交业务} {true 0 cls_11 金融业务}]
	str2 := "[{\"open\": \"true\",\"pId\": 0,\"id\": \"cls_1\",\"name\": \"\\u793e\\u4ea4\\u4e1a\\u52a1\"},{\"open\": \"true\",\"pId\": 0,\"id\": \"cls_11\",\"name\": \"\\u91d1\\u878d\\u4e1a\\u52a1\"}]"
	type Str2 struct {
		PId  int    `json:"pId"`
		Name string `json:"name"`
	}
	res2 := []Str2{}
	_ = json.Unmarshal([]byte(str2), &res2)
	fmt.Println(res2) // [{0 社交业务} {0 金融业务}]
}

// Json9 .
func Json9() {
	marshal, err := json.Marshal(nil)
	if err != nil {
		fmt.Println("error")
		panic(err)
	}
	fmt.Printf("%+v\n", marshal) // [110 117 108 108]
}

// Json10 json转数字类型
func Json10(str string) {
	type test struct {
		ID   interface{} `json:"id"`
		Name string      `json:"name"`
	}
	t := test{}
	err := json.Unmarshal([]byte(str), &t)
	if err != nil {
		panic(err)
	}
	fmt.Printf("id:%v, type:%T \n", t.ID, t.ID)
}
