package lib

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestJson1(t *testing.T) {
	Json1()
}

func TestJson2(t *testing.T) {
	Json2()
}

func TestJson3(t *testing.T) {
	Json3()
}

func TestJson4(t *testing.T) {
	Json4()
}

func TestJson5(t *testing.T) {
	Json5()
}

func TestJson6(t *testing.T) {
	Json6()
}

func TestJson7(t *testing.T) {
	Json7()
}

func TestJson8(t *testing.T) {
	// Json8()
	type rsp struct {
		Code int    `json:"code"`
		Msg  string `json:"msg"`
		Type int    `json:"type"`
	}
	/*body:=rsp{Code: 100,Msg: "111"}
	marshal, err := json.Marshal(&body)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(marshal))*/
	r := &rsp{}
	body := "{\"Code\":100,\"Msg\":\"111\",\"TTT\":\"111\"}"
	err := json.Unmarshal([]byte(body), r)
	if err != nil {
		panic(err)
	}
	fmt.Println(r)
}

func TestJson10(t *testing.T) {
	Json10(`{"id":123,"name":"test1"}`)
	Json10(`{"id":1,"name":"test1"}`)
	Json10(`{"id":132523,"name":"test1"}`)
	Json10(`{"id":1214236234,"name":"test1"}`)
	Json10(`{"id":266,"name":"test1"}`)
	Json10(`{"id":255,"name":"test1"}`)
	Json10(`{"id":18,"name":"test1"}`)
}
