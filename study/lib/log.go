/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/8 19:42
 */

package lib

import (
	"fmt"
	"log"
	"os"
)

// log库无法满足记录不同级别的日志

// Log1 打印日志到终端
func Log1() {
	log.Println("普通日志")
	// Fatal系列函数会在写入日志信息后调用os.Exit(1)
	log.Fatalln("fatal级别日志")
	// Panic系列函数会在写入日志信息后panic。
	log.Panicln("panic日志")
}

/** log.Flags():日志输出格式 可以自己配置
const (
	// 控制输出日志信息的细节，不能控制输出的顺序和格式。
	// 输出的日志在每一项后会有一个冒号分隔：例如2009/01/23 01:23:23.123123 /a/b/c/d.go:23: message
	Ldate         = 1 << iota     // 日期：2009/01/23
	Ltime                         // 时间：01:23:23
	Lmicroseconds                 // 微秒级别的时间：01:23:23.123123（用于增强Ltime位）
	Llongfile                     // 文件全路径名+行号： /a/b/c/d.go:23
	Lshortfile                    // 文件名+行号：d.go:23（会覆盖掉Llongfile）
	LUTC                          // 使用UTC时间
	LstdFlags     = Ldate | Ltime // 标准logger的初始值
)
*/
func Log2() {
	log.SetFlags(log.Ldate | log.Ltime | log.Llongfile)
	// 2021/09/08 17:45:39 E:/golang/GoWorks/helloworld/study/lib/log.go:28: 查看我设置的日志格式
	log.Println("查看我设置的日志格式")
}

// log.Prefix()：设置日志前缀
func Log3() {
	log.SetPrefix("[lena]")
	// [lena]
	fmt.Println(log.Prefix())
	// [lena]2021/09/08 17:50:48 添加前缀后的效果
	log.Println("添加前缀后的效果")
}

// 设置日志输出位置
func Log4() {
	file, err := os.OpenFile("./exercise/logger/logFile/20210908.log", os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		fmt.Println("open err :", err)
		return
	}
	log.SetOutput(file)
	log.Println("查看是否输出在文件中")
	// 2021/09/08 17:55:41 查看是否输出在文件中
}

// 可以通过new自定义Logger
func Log5() {
	// func New(out io.Writer日志输出位置, prefix string日志前缀, flag int日志格式) *Logger
	logger := log.New(os.Stdout, "[logger]", log.Ldate|log.Ltime|log.Llongfile)
	// [logger]2021/09/08 18:05:17 E:/golang/GoWorks/helloworld/study/lib/log.go:63: test
	logger.Println("test")
}
