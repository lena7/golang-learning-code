/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/5 19:42
 */

package lib

import (
	"fmt"
	"os"
)

// OS1 获取当前文件路径
func OS1() {
	str, _ := os.Getwd() // E:\golang\GoWorks\helloworld
	fmt.Println(str)
}

// OS2 os.Args:获取命令行参数
func OS2() {
	// 如输入 lib/Args1.exe helloworld 那么命令行参数是 helloworld
	if len(os.Args) > 2 {
		for index, value := range os.Args {
			fmt.Println(index, value)
		}
	}
}

// OS3 os.exit(code int) 传入code支持[0,125]的值
func OS3() {
	defer fmt.Println("over!")
	// 0 表示成功退出 会抛出panic 正常执行defer
	os.Exit(0)
	// 非0 表示出错退出 会让程序立即退出 defer函数也不会执行
	//os.Exit(1)
}
