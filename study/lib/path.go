/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/7 19:42
 */

package lib

import (
	"fmt"
	"os"
	"path"
	"time"
)

// Path1 路径拼接path.Join
func Path1() {
	// 获取当前日期
	now := time.Now()
	filename := now.Format("20060102") + ".log"
	// 获取日志路径
	road, _ := os.Getwd()
	newPath := path.Join(road, "./exercise/logger/logFile", filename)
	fmt.Println(newPath) // E:\golang\GoWorks\helloworld/exercise/logger/logFile/20210907.log
	file, _ := os.OpenFile(newPath, os.O_CREATE|os.O_APPEND, 0644)
	// 成功创建文件
	defer file.Close()
}

// Path2 在路径中拿到文件名:path.Base
func Path2() {
	now := time.Now()
	filename := now.Format("20060102") + ".log"
	// 获取日志路径
	road, _ := os.Getwd()
	fmt.Printf("road:%s\n", road) // road:/Users/lena/GoWork/golang/golang-learning-code/study/lib
	newPath := path.Join(road, "./exercise/logger/logFile", filename)
	fmt.Printf("newPath:%s\n", newPath) // newPath:/Users/lena/GoWork/golang/golang-learning-code/study/lib/exercise/logger/logFile/20220706.log
	s := path.Base(newPath)
	fmt.Printf("s:%s\n", s) // 20210907.log
}
