/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/9 19:42
 */

package lib

import (
	"fmt"
	"math/rand"
	"time"
)

// 随机生成一个int64型数字
func Rand1() {
	int63 := rand.Int63()
	fmt.Println(int63)
	fmt.Printf("%T", int63)
}

/** 猜数游戏
随机生成一个1-100的整数，有十次机会猜中
如果第一次就猜中，提示“你真是个天才”
如果第2-3次猜中，提示“你很聪明，赶上我了”
如果第4-9次猜中，提示“一般般”
如果最后一次猜中，提示“可算猜对啦”
如果一次都没猜中，提示“说你点啥好呢”
*/
func GuessNum() {
	// 生成1-100的整数
	rand.Seed(time.Now().Unix()) // 设置rand的种子
	res := rand.Intn(100)
	//fmt.Println(res)
	for i := 1; i < 11; i++ {
		fmt.Printf("请输入你猜的结果：")
		cur := 0
		fmt.Scanln(&cur)
		if cur == res {
			switch i {
			case 1:
				fmt.Println("你真是个天才")
			case 2, 3:
				fmt.Println("你很聪明，赶上我了")
			case 4, 5, 6, 7, 8, 9:
				fmt.Println("一般般")
			case 10:
				fmt.Println("可算猜对啦")
			}
			return
		}
	}
	fmt.Println("说你点啥好呢")
}
