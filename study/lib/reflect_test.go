package lib

import (
	"fmt"
	"reflect"
	"testing"
)

type a int

func TestReflect1(t *testing.T) {
	data := a(1)
	Reflect(data)
}

func TestReflect(t *testing.T) {
	str := AVF{1, "a"}
	// str := ""
	data := reflect.ValueOf(str)
	fmt.Printf("%v\n", data)

	val := data.MethodByName("Data")
	fmt.Printf("get %+v\n", val)
	b := val.Call(nil)
	fmt.Printf("1 %+v\n", b)
	fmt.Printf("2 %+v\n", reflect.Value{})
	fmt.Printf("t %+v\n", val.IsValid())
	fmt.Printf("t %+v\n", val.Interface())
	fmt.Printf("f %+v\n", b[0])
	fmt.Printf("e %+v\n", b[0].Interface())
}

type AVF struct {
	a int
	b string
}

func TestReflectGetValueByKey(t *testing.T) {
	a := &AVF{1, "data"}
	value := ReflectGetValueByKey(a, "a")
	fmt.Println(value)
}

func (a AVF) Data() {
	fmt.Println("data")
}
