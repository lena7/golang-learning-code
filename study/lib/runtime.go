/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/7 19:42
 */

package lib

import (
	"fmt"
	"runtime"
)

// caller方法用于定位当前文件和行 可传入int数字 0=当前行 1=上一个调用该行所在方法的行 2=再上上一层
func Runtime1() {
	_, file, line, ok := runtime.Caller(0)
	if !ok {
		fmt.Println("caller err")
	}
	fmt.Println(file, line)
}

func Runtime2() {
	_, file, line, ok := runtime.Caller(1)
	if !ok {
		fmt.Println("caller err")
	}
	fmt.Println(file, line)
}
func Runtime3() {
	// 查看caller输出是哪一行
	Runtime2()
}

func Runtime4() {
	_, file, line, ok := runtime.Caller(2)
	if !ok {
		fmt.Println("caller err")
	}
	fmt.Println(file, line)
}
func Runtime5() {
	Runtime4()
}
func Runtime6() {
	// 查看caller输出是哪一行
	Runtime5()
}
