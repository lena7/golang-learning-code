/**
 * @Author: lena
 * @Description:sort
 * @Version: 1.0.0
 * @Date: 2021/9/15 21:07
 */

package lib

import (
	"fmt"
	"sort"
)

// Sort1 []int排序
func Sort1() {
	arr := []int{12, 4, 6, 2, 8, 11, 14, 7, 88, 4, 33, 11, 24, 18, 3}
	sort.Ints(arr)
	fmt.Println(arr) // [2 3 4 4 6 7 8 11 11 12 14 18 24 33 88]
}

// BizInfo 定义结构体排序规则
type BizInfo struct {
	Biz string `json:"biz"`
	// OrangeFlag int     `json:"orange_flag"`
	Score float64 `json:"score"`
}

// BizInfoSlice 切片
type BizInfoSlice []BizInfo

func (bis BizInfoSlice) Len() int {
	return len(bis)
}

func (bis BizInfoSlice) Swap(i, j int) {
	bis[i], bis[j] = bis[j], bis[i]
}

func (bis BizInfoSlice) Less(i, j int) bool {
	return bis[i].Score > bis[j].Score // 按score的值 降序排序
}

// Sort2 测试切片排序
func Sort2() {
	bis := []BizInfo{{"a", 99.2}, {"b", -1}, {"c", 83.93}, {"d", 0}}
	fmt.Println(bis)
	sort.Sort(BizInfoSlice(bis))
	fmt.Println(bis)
}

// Sort3 二分查找 假定数组strArray已排序
func Sort3(strArray []string, target string) bool {
	//fmt.Printf("%s in is ", target)
	index := sort.SearchStrings(strArray, target)
	fmt.Printf("%s in is %d\n", target, index)
	if index < len(strArray) && strArray[index] == target {
		return true
	}
	return false
}

func Sort4(strArray []string) []string {
	sort.Strings(strArray)
	return strArray
}
