package lib

import (
	"fmt"
	"sort"
	"testing"
)

func TestSort2(t *testing.T) {
	Sort2()
}

func TestSort3(t *testing.T) {
	str := []string{"lenalin", "nihao", "ads", "bsc", "rwx", "csi"}
	sort.Strings(str)
	fmt.Println(Sort3(str, "ad"))
	fmt.Println(Sort3(str, "nihao"))
	fmt.Println(Sort3(str, "bsc"))
	fmt.Println(Sort3(str, "lena"))
	fmt.Println(Sort3(str, "rwx"))
	fmt.Println(Sort3(str, "lenalin"))
	fmt.Println(Sort3(str, "csi"))
	fmt.Println(Sort3(str, "nihao"))
	fmt.Println(Sort3(str, "ads"))
}

func TestSort4(t *testing.T) {
	str := []string{"", "ads", "nihao", "ads", "", "bsc", "rwx", "", "csi"}
	res := Sort4(str)
	fmt.Printf("%+v\n", res)
	fmt.Println(len(res))
	index := 0
	for _, n := range res {
		if n == "" {
			index++
		} else {
			break
		}
	}
	if index != 0 {
		res = res[index:]
	}
	fmt.Printf("%+v\n", res)
	fmt.Println(len(res))
}
