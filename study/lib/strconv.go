/**
 * @Author: lena
 * @Description:strconv该库用于强制类型转换
 * @Version: 1.0.0
 * @Date: 2021/9/9 19:42
 */

package lib

import (
	"fmt"
	"strconv"
)

// Strconv1 string -> int :strconv.ParseInt
func Strconv1() {
	s := "10000"
	// func ParseInt(s string, base int, bitSize int) (i int64, err error)
	// 将s转为int型 十进制 64位
	parseInt, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		fmt.Println("turn err :", err)
		return
	}
	// 值为10000,类型为int64
	fmt.Printf("值为%v,类型为%T", parseInt, parseInt)
	// 注意：返回int值默认是64位的 但可以通过指定限制实际数据使用的位数
}

// Strconv2 string -> int :strconv.Atoi
func Strconv2(s string) {
	atoi, err := strconv.Atoi(s)
	if err != nil {
		fmt.Println("turn err :", err)
		return
	}
	// 值为10000,类型为int
	fmt.Printf("值为%v,类型为%T\n", atoi, atoi)
}

// Strconv3 string -> bool :strconv.ParseBool
func Strconv3() {
	b := "true"
	parseBool, err := strconv.ParseBool(b)
	if err != nil {
		fmt.Println("turn err :", err)
		return
	}
	// 值为true,类型为bool
	fmt.Printf("值为%v,类型为%T", parseBool, parseBool)
}

// Strconv4 string -> float :strconv.ParseFloat
func Strconv4() {
	s := "1.111"
	// func ParseFloat(s string, bitSize int) (float64, error)
	float, err := strconv.ParseFloat(s, 64)
	if err != nil {
		fmt.Println("turn err :", err)
		return
	}
	// 值为1.111,类型为float64
	fmt.Printf("值为%v,类型为%T", float, float)
}

// Strconv5 int -> string :strconv.Itoa
func Strconv5() {
	i := 10000
	itoa := strconv.Itoa(i)
	// 值为10000,类型为string
	fmt.Printf("值为%v,类型为%T", itoa, itoa)
}

// Strconv6 进制转换：10进制转2,8,16进制
func Strconv6() {
	// 返回string：func FormatInt(i int64, base int) string
	int2 := strconv.FormatInt(128, 2)
	fmt.Println(int2) // 10000000
}

// Strconv7 string -> []byte
func Strconv7() {
	s := "helloworld"
	b := []byte(s)
	fmt.Println(b) // [104 101 108 108 111 119 111 114 108 100]
}

// Strconv8 []byte -> string
func Strconv8() {
	b := []byte{104, 101, 108, 108, 111, 119, 111, 114, 108, 100}
	s := string(b)
	fmt.Println(s) // helloworld
}

// Strconv9 float64 -> string
func Strconv9() {
	num := 100.00
	str := strconv.FormatFloat(num, 'g', 1, 64)
	fmt.Println(str)
}
