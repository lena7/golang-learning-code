/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/1 19:54
 */

package lib

import (
	"fmt"
	"strings"
)

// EqualFold 不区分大小写 全匹配才返回true
func EqualFold(str1, str2 string) {
	fold := strings.EqualFold(str1, str2)
	fmt.Printf("%s 与 %s = %v\n", str1, str2, fold)
}

// ReplaceStringN strings.Replace(字符串,要替代的字符串,替换成的字符串,替换个数) : 替换字符串,个数=-1表示全部替换
func ReplaceStringN() {
	s := "abcabcabcabcabcabc"
	all := strings.Replace(s, "b", "i", 2)
	fmt.Println(all) // aicaicabcabcabcabc
}

// ReplaceStringAll strings.ReplaceAll(字符串,要替代的字符串,替换成的字符串) : 替换所有匹配字符串
func ReplaceStringAll() {
	s := "abcabcabcabcabcabc"
	all := strings.ReplaceAll(s, "b", "i")
	fmt.Println(all) // aicaicaicaicaicaic
}

// JoinString strings.Join([]string,string) : 将字符数组按指定字符拼接起来
func JoinString() {
	s := []string{"abc", "def", "ghi", "jkl", "mno", "pqr"}
	res := strings.Join(s, ",") // 以","拼接起来s
	fmt.Println(res)            // abc,def,ghi,jkl,mno,pqr
}

// Contain strings.Contains(string,string) : 查看字符串是否包含指定字符串
func Contain(s, taget string) bool {
	return strings.Contains(s, taget)
}

// ContainCount strings.Count(string,string) : 统计字符串有几个子串
func ContainCount(s, target string) int {
	return strings.Count(s, target)
}

// FirstIndex strings.Index(string,string) : 返回字符串第一次出现的位置,没有返回-1
func FirstIndex(s, target string) int {
	return strings.Index(s, target)
}

// LastIndex strings.LastIndex(string,string) : 返回字符串最后一次出现的位置,没有返回-1
func LastIndex(s, target string) int {
	return strings.LastIndex(s, target)
}

// Trim strings.Trim(s,cutset) : 去掉字符串s中首部以及尾部与字符串cutset中每个相匹配的字符
func Trim() {
	s := "你好hello你好呀！#！0" +
		"hello是我啊！！@￥"
	trim := strings.Trim(s, "hello")
	fmt.Println(trim) // 你好呀！#！0hello是我啊！！@￥
	// 注意：在中间的不做处理 只处理首部和尾部
}

// ToUpper strings.ToUpper : 将字符串转换成大写
func ToUpper() {
	s := "hello"
	upper := strings.ToUpper(s)
	fmt.Println(upper) // HELLO
}

// IsEquals == : 判断两个字符串是否相等,区分大小写
func IsEquals(s1, s2 string) bool {
	return s1 == s2 // 内容相等即相等
}

// IsEquals2 strings.EqualFoled : 不区分大小写的字符串比较
func IsEquals2(s1, s2 string) bool {
	return strings.EqualFold(s1, s2)
}

// SplitStr strings.Split(string,string)：切割字符串
func SplitStr(str string) []string {
	if str == "" {
		return []string{}
	}
	return strings.Split(str, ";")
}

// HeadToUpperOrLower 字符串字母大小写转换
func HeadToUpperOrLower() {
	s := "go"
	fmt.Println(s) // go
	upper := strings.ToUpper(s)
	fmt.Println(upper) // GO
	lower := strings.ToLower(upper)
	fmt.Println(lower) // go
}

// DelBlank 去掉字符串左右两边空格：TrimSpace
func DelBlank() {
	s := "  你好，我是傻逼许嘉志   "
	space := strings.TrimSpace(s)
	fmt.Println(space) // 你好，我是傻逼许嘉志
}

// DelStr 去掉字符串左右两边指定字符：Trim
func DelStr() {
	s := "!hello,i am lena!,!"
	trim := strings.Trim(s, "!")
	fmt.Println(trim) // hello,i am lena!,
}

// DelLeftOrRightStr 去掉左边/右边指定字符：TrimLeft/TrimRight
func DelLeftOrRightStr() {
	s := "hello lena!"
	left := strings.TrimLeft(s, "hello")
	fmt.Println(left) //  lena!
	right := strings.TrimRight(left, "!")
	fmt.Println(right) //  lena
}

// PrefixOrSuffix 判断字符串是否以特定字符串开头/结尾：HasPrefix/HasSuffix
func PrefixOrSuffix() {
	s := "20210914.log"
	prefix := strings.HasPrefix(s, "202109")
	fmt.Println(prefix) // true
	suffix := strings.HasSuffix(s, ".log")
	fmt.Println(suffix) // true
}

// addSuffix *若文件不包含指定后缀suffix,给文件添加后缀name+suffix
func addSuffix(suffix string) func(string) string {
	return func(name string) string {
		// HasSuffix(文件名,后缀) => 某一文件名是否以指定后缀结尾
		if !strings.HasSuffix(name, suffix) {
			return name + suffix
		}
		return name
	}
}

// WordAppendTimes *判断传入单词出现的次数:没有校验是否是单词,默认全部由单词和空格组成
func WordAppendTimes(s string) (res map[string]int) {
	res = make(map[string]int)
	// 按空格分割字符串
	sarray := strings.Split(s, " ")
	for _, cur := range sarray {
		res[cur]++
	}
	return
}

// SplitHanZi 分割汉字字符串
func SplitHanZi(s string) {
	ss := strings.Split(s, "")
	for _, cur := range ss {
		fmt.Println(cur)
	}
}

// IsHuiWen1 *判断是否是回文 方式一：通过前后字符依次比对
func IsHuiWen1(s string) bool {
	for i := 0; i < len(s)/2; i++ {
		// 注意：只能用来判断英文,中文一个字占位不止1
		if s[i] != s[len(s)-i-1] {
			return false
		}
	}
	return true
}

// IsHuiWen2 方式二：分割后再进行头尾比较
func IsHuiWen2(s string) bool {
	// 先按字符进行分割,这样无论输入的字符是何种形式都能正确判断
	ss := strings.Split(s, "")
	for i := 0; i < len(ss)/2; i++ {
		if ss[i] != ss[len(ss)-i-1] {
			return false
		}
	}
	return true
}

// IsHuiWen3 方式三：反转字符串后比较
func IsHuiWen3(s1 string) bool {
	ss := strings.Split(s1, "")
	// 字符串反转:从后往前拼接
	var s2 string
	for i := len(ss) - 1; i >= 0; i-- {
		s2 += ss[i]
	}
	return s1 == s2
}

// SplitString *按seq切割s字符串 如s=abacdebsnb,seq=b,return=[a,acde,sn,] 注意：seq不一定只有一个字符
func SplitString(s, seq string) []string {
	var result []string
	// 获取s中第一个seq的下标
	i := strings.Index(s, seq)
	for i > -1 {
		result = append(result, s[:i])
		// s变为seq后半部分
		s = s[i+1:]
		i = strings.Index(s, seq)
	}
	result = append(result, s)
	return result
}
