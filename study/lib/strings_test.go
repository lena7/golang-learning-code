/**
 * @Author: lena
 * @Description:strings的测试方法
 * @Version: 1.0.0
 * @Date: 2021/9/12 13:47
 */

package lib

import (
	"fmt"
	"reflect"
	"runtime"
	"strings"
	"testing"
)

func TestDelBlank(t *testing.T) {
	DelBlank()
}

func TestEqualFold(t *testing.T) {
	str1 := "lena"
	str2 := "Lena"
	str3 := "lenalin"
	str4 := "lenali"
	str5 := "lenalix"
	EqualFold(str1, str3)
	EqualFold(str2, str3)
	EqualFold(str3, str4)
	EqualFold(str3, str5)
	EqualFold(str1, str2)
}

func TestSplitStr(t *testing.T) {
	fmt.Printf("%+v\n", len(SplitStr("nihap;wo")))
	fmt.Printf("%+v\n", len(SplitStr("nihap;")))
	fmt.Printf("%+v\n", len(SplitStr("")))
}

type tests struct {
	s      string
	target string
	res    interface{}
}

func TestContain(t *testing.T) {
	test := []tests{
		{"abcd", "a", true},
		{"abcd", "b", true},
		{"abcd", "e", false},
		{"abcde", "bc", true},
		{"abcde", "bd", false},
		{"你好啊", "啊", true},
		{"你好啊", "阿", false},
		{"你好 啊", "啊", true},
		{"你好,啊", ",", true},
	}
	for _, ts := range test {
		t.Run(ts.s, func(t *testing.T) {
			res := Contain(ts.s, ts.target)
			if !reflect.DeepEqual(res, ts.res) {
				t.Errorf("%v find %v error", ts.s, ts.target)
			}
		})
	}
}

func TestContainCount(t *testing.T) {
	test := []tests{
		{"abcda", "a", 2},
		{"abcd", "b", 1},
		{"abcdfedaabvdrw0", "a", 3},
		{"abcd", "e", 0},
		{"abcde", "bc", 1},
		{"abcde", "bd", 0},
		{"abdkrabfeeabeweba", "ab", 3},
		{"你好啊", "啊", 1},
		{"你好啊", "阿", 0},
		{"你好 啊", "啊", 1},
		{"你好,啊", ",", 1},
	}
	for _, ts := range test {
		t.Run(ts.s, func(t *testing.T) {
			res := ContainCount(ts.s, ts.target)
			if !reflect.DeepEqual(res, ts.res) {
				t.Errorf("%v find %v count %v error,expert count %v", ts.s, ts.target, res, ts.res)
			}
		})
	}
}

func TestFirstIndex(t *testing.T) {
	test := []tests{
		{"abcda", "a", 0},
		{"abcd", "b", 1},
		{"abcdfedaabvdrw0", "a", 0},
		{"abcd", "e", -1},
		{"abcde", "bc", 1},
		{"abcde", "bd", -1},
		{"abdkrabfeeabeweba", "ab", 0},
		{"你好啊", "啊", 6},
		{"你好啊", "阿", -1},
		{"你好 啊", "啊", 7},
		{"你好,啊", ",", 6},
	}
	for _, ts := range test {
		t.Run(ts.s, func(t *testing.T) {
			res := FirstIndex(ts.s, ts.target)
			if !reflect.DeepEqual(res, ts.res) {
				t.Errorf("%v find %v first index %v error,expert index is %v", ts.s, ts.target, res, ts.res)
			}
		})
	}
}

func TestLastIndex(t *testing.T) {
	test := []tests{
		{"abcda", "a", 4},
		{"abcd", "b", 1},
		{"abcdfedaabvdrw0", "a", 8},
		{"abcd", "e", -1},
		{"abcde", "bc", 1},
		{"abcde", "bd", -1},
		{"abdkrabfeeabeweba", "ab", 10},
		{"你好啊", "啊", 6},
		{"你好啊", "阿", -1},
		{"你好 啊", "啊", 7},
		{"你好,啊", ",", 6},
	}
	for _, ts := range test {
		t.Run(ts.s, func(t *testing.T) {
			res := LastIndex(ts.s, ts.target)
			if !reflect.DeepEqual(res, ts.res) {
				t.Errorf("%v find %v last index %v error,expert index is %v", ts.s, ts.target, res, ts.res)
			}
		})
	}
}

func TestIsEquals2(t *testing.T) {
	type tests struct {
		s1  string
		s2  string
		res bool
	}
	test := []tests{
		{"abc", "ABC", true},
		{"aBS", "Abs", true},
		{"abs", "abc", false},
		{"你好啊", "你好啊", true},
	}
	for _, ts := range test {
		t.Run(ts.s1, func(t *testing.T) {
			res := IsEquals2(ts.s1, ts.s2)
			if !reflect.DeepEqual(res, ts.res) {
				t.Errorf("s1=%v,s2=%v,res=%v,expert=%v", ts.s1, ts.s2, res, ts.res)
			}
		})
	}
}

func Test1WordAppendTimes(t *testing.T) {
	res := WordAppendTimes("how are you lena")
	m := map[string]int{
		"are":  1,
		"how":  1,
		"lena": 1,
		"you":  1,
	}
	if !reflect.DeepEqual(res, m) {
		t.Errorf("expert is %v , but result is %v", m, res)
	}
}

func Test2WordAppendTimes(t *testing.T) {
	res := WordAppendTimes("what do you do now")
	m := map[string]int{
		"do":   2,
		"now":  1,
		"what": 1,
		"you":  1,
	}
	if !reflect.DeepEqual(res, m) {
		t.Errorf("expert is %v , but result is %v", m, res)
	}
}

func TestIsHuiWen1(t *testing.T) {
	res := IsHuiWen1("whattahw")
	if !reflect.DeepEqual(res, true) {
		t.Errorf("error1")
	}
}

func TestIsHuiWen2(t *testing.T) {
	// 使用测试组
	type test struct {
		s      string // 输入参数
		expert bool   // 预期返回值
	}
	tests := []test{
		{"hello你好好呀olleh", false},
		{"上海海上", true},
		{"hello你好好你olleh", true},
	}
	// 遍历结果
	for _, ts := range tests {
		res := IsHuiWen2(ts.s)
		if !reflect.DeepEqual(res, ts.expert) {
			t.Errorf("error input : %v", ts.s)
		}
	}
}

func TestIsHuiWen3(t *testing.T) {
	// 使用子测试：可以单独测试 测试组中的某一个用例
	type test struct {
		s      string // 输入参数
		expert bool   // 预期返回值
	}
	tests := []test{
		{"hello你好好呀olleh", false},
		{"上海海上", true},
		{"hello你好好你olleh", true},
		{"whattahw", true},
	}
	for _, ts := range tests {
		t.Run(ts.s, func(t *testing.T) {
			res := IsHuiWen2(ts.s)
			if !reflect.DeepEqual(res, ts.expert) {
				t.Errorf("error input : %v", ts.s)
			}
		})
	}

}

func TestSplitString(t *testing.T) {
	res := SplitString("abacdebsnb", "b")
	expert := []string{"a", "acde", "sn", ""}
	if !reflect.DeepEqual(res, expert) {
		t.Errorf("expert is %v , but result is %v", expert, res)
	}
}

func TestLenStr(t *testing.T) {
	s := ""
	defer func() {
		if r := recover(); r != nil {
			buf := make([]byte, 1<<24)
			buf = buf[:runtime.Stack(buf, false)]
			fmt.Printf("res:%s", s)
		}
	}()
	for {
		s += "999"
		fmt.Printf("==%s\n", s)
	}
}

func Test1(t *testing.T) {
	flag := strings.Contains("400 bad request for {:name=>[\\\"Branch name exists\\", "Branch name exists")
	fmt.Println(flag)
}
