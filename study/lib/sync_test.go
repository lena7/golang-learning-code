package lib

import (
	"sync"
	"testing"
	"time"
)

func TestOnce1(t *testing.T) {
	once := &sync.Once{}
	go Once1(once)
	go Once1(once)
	go Once1(once)
	time.Sleep(time.Second * 1)
}

func TestMap1(t *testing.T) {
	Map1()
}

func TestLock1(t *testing.T) {
	Lock1()
}

func TestLock2(t *testing.T) {
	Lock2()
}
