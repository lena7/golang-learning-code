package main

import (
	"bytes"
	"fmt"
	"text/template"
)

func main() {
	s := study{
		ID:       12,
		Password: "1234",
		Datas:    []string{"data1", "data2", "data3"},
	}
	temp := template.Must(template.New("test").Parse(model))
	res := bytes.Buffer{}
	err := temp.Execute(&res, s)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\n", res.String())
}

type study struct {
	ID       int
	Password string
	Datas    []string
}

/**
{{.xxx}} 标明获取xxx的值

{{range $索引key,$值key := .xxx}}{{if $索引key}}[可选内容：每个遍历的后缀]{{end}} 表示遍历xxx数组
{{$index}} 获取这一次索引 从0开始
{{$data}} 获取data对应的value
{{end}} 结束遍历
*/
const model = `
	my id is {{.ID}}
	my pass is {{.Password}}
	{{range $index,$data := .Datas}}{{if $index}}完毕{{end}}
	datas is {{$index}} {{$data}} ;{{end}}
`
