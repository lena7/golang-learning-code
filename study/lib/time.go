/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/6 20:45
 */

package lib

import (
	"fmt"
	"time"

	"helloworld/study/process"
	"helloworld/tools/timex"
)

// Time1 获取当前年月日时分秒
func Time1() {
	// func Now() Time
	now := time.Now()
	fmt.Println(now)          // 2021-09-06 20:49:28.771279 +0800 CST m=+0.008246701
	fmt.Println(now.Year())   // 2021
	fmt.Println(now.Month())  // September
	fmt.Println(now.Day())    // 6
	fmt.Println(now.Hour())   // 20
	fmt.Println(now.Minute()) // 47
	fmt.Println(now.Second()) // 44
}

// Time2 获取当前时间戳
func Time2() {
	now := time.Now()            // 获取当前时间
	timestamp1 := now.Unix()     // 时间戳
	timestamp2 := now.UnixNano() // 纳秒时间戳
	fmt.Println(timestamp1)      // 1630933878
	fmt.Println(timestamp2)      // 1630933878901505100
}

// Time3 将时间戳转为时间格式
func Time3() {
	unix := time.Unix(1630933878, 0)
	fmt.Println(unix) // 2021-09-06 21:11:18 +0800 CST
}

// Time4 小时数增加
func Time4() {
	now := time.Now()
	fmt.Println(now) // 2021-09-06 21:15:33.6532234 +0800 CST m=+0.008447101
	t := now.Add(time.Hour * 5)
	fmt.Println(t) // 2021-09-07 02:15:33.6532234 +0800 CST m=+18000.008447101
}

// Time15 获取昨天
func Time15() {
	now := time.Now()
	yes := now.AddDate(0, 0, -1)
	format := yes.Format("2006-01-02")
	fmt.Println(format) // 2021-11-17
}

// Time5 计算两数之间的差值
func Time5() {
	now := time.Now()
	fmt.Println(now)
	t := now.Add(time.Hour * 5)
	sub := now.Sub(t)
	fmt.Println(sub) // -5h0m0s (5hour,0minutes,0second)
}

// Time6 判断两个时间是否相同，会考虑时区的影响，因此不同时区标准的时间也可以正确比较。本方法和用t==u不同，这种方法还会比较地点和时区信息。
func Time6() {
	now := time.Now()
	t := now.Add(time.Hour * 5)
	equal := now.Equal(t)
	fmt.Println(equal) // false
}

// Time7 设置定时器:本质是一个channel通道
func Time7() {
	ticker := time.Tick(time.Second) // 定义一个1秒间隔的定时器
	for i := range ticker {
		fmt.Println(i) // 每秒都会执行的任务
	}
}

// Time8 时间格式化:格式化的模板为Go的出生时间2006年1月2号15点04分 Mon Jan
func Time8() {
	now := time.Now()
	fmt.Println(now)                      // 2021-09-06 21:27:57.0772976 +0800 CST m=+0.007087801
	fmt.Println(now.Format("2006-01-02")) // 2021-09-06
}

// Time9 格式化解析时间
func Time9() {
	now := time.Now()
	fmt.Println(now) // 2021-09-06 21:30:15.3328925 +0800 CST m=+0.006978101
	// 加载时区：根据市区加载时间
	loc, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(loc) // Asia/Shanghai
	// 按照指定时区和指定格式解析字符串时间:格式化的模板为Go的出生时间2006年1月2号15点04分 Mon Jan
	timeObj, err := time.ParseInLocation("2006/01/02", "2019/08/04", loc)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(timeObj)          // 2019-08-04 00:00:00 +0800 CST
	fmt.Println(timeObj.Sub(now)) // -18357h32m0.059821s
}

// Time10 作业：获取当前时间，格式化输出为2017/06/19 20:30:05`格式。
func Time10() {
	now := time.Now()
	fmt.Println(now)                               // 2021-09-06 21:34:06.9467295 +0800 CST m=+0.010308501
	fmt.Println(now.Format("2006/01/02 15:04:05")) // 2021/09/06 21:34:06
}

// Time11 作业：编写程序统计一段代码的执行耗时时间，单位精确到微秒。
func Time11() {
	start := time.Now()
	fmt.Println(start) // 2021-09-06 21:36:10.4767606 +0800 CST m=+0.007113301
	process.Goto1()
	end := time.Now()
	fmt.Println(end)            // 2021-09-06 21:36:10.5044471 +0800 CST m=+0.034799801
	fmt.Println(end.Sub(start)) // 27.6865ms
}

// Time12 判断是否是同一天
func Time12() {
	day1 := time.Now()
	fmt.Println(day1)
	day2 := time.Now()
	if day1.Day() == day2.Day() && day1.Month() == day2.Month() && day1.Year() == day2.Year() {
		fmt.Println("equals")
	}
}

// Time13 UTC：作用
func Time13() {
	now := time.Now()
	fmt.Println(now) // 2021-09-14 16:56:31.4616573 +0800 CST m=+0.017492601
	utc := now.UTC()
	fmt.Println(utc) // 2021-09-14 08:56:31.4616573 +0000 UTC
}

// Time14 string -> time ：将字符串转换为时间
func Time14() {
	prefix := "2021-1-1 10:31:12"
	moudle := "2006-1-2 15:04:05"
	// func Parse(layout, value string) (Time, error)
	parse, err := time.Parse(moudle, prefix)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(parse) // 2021-01-01 10:31:12 +0000 UTC
}

// Time16 获取开始结束时间
func Time16() {
	start := time.Now()
	time.Sleep(time.Second)
	cost := time.Since(start)
	fmt.Println("消耗时间：", cost)
}

func Time17(t string) {
	tt, _ := timex.ParseTime(t)
	fmt.Printf("%s 星期%d\n", t, tt.Weekday())
}

/** 时间常量
Hour 小时 = 60分钟
Minute 分钟 = 60秒
Second 秒 = 1000毫秒
Millisecond 毫秒  = 1000微秒
Microsecond 微妙  = 1000纳秒
Nanosecond  纳秒  = Duration = 1
*/

// FormatTime 格式化时间：年-月-日 时:分:秒
func FormatTime() {
	now := time.Now()
	// 2021-9-14 16:58:33
	fmt.Printf("%d-%d-%d %d:%d:%d", now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
}

// Print100OnceSecond *每隔1s打印一个数字，打印到100的时候退出
func Print100OnceSecond() {
	i := 0
	for {
		i++
		fmt.Println(i)
		time.Sleep(time.Second)
		if i >= 100 {
			break
		}
	}
	fmt.Println("打印结束！")
}

// Print100OncePercentSecond *每隔0.1s打印一个数字，打印到100时就突出
func Print100OncePercentSecond() {
	i := 0
	for {
		i++
		fmt.Println(i)
		time.Sleep(time.Millisecond * 100)
		if i >= 100 {
			break
		}
	}
	fmt.Println("打印结束！")
}

// FishOrNet *从1990年1月1日开始执行“三天打鱼两天晒网”,输入一个日期,判断当天是打鱼还是晒网
func FishOrNet() {
	start, err := time.Parse("2006-01-02", "1990-01-01")
	// 获取输入的日期
	fmt.Printf("请输入格式为1990-01-01的时间：")
	var str string
	fmt.Scanln(&str)
	// 将输入的时间转为time类型
	t, err := time.Parse("2006-01-02", str)
	if err != nil {
		fmt.Println("你输入的日期不符合格式要求:", err)
		return
	}
	// 计算输入日期和1990-1-1相差的天数
	duration := t.Sub(start)
	days := duration.Hours()/24 + 1 // 第一天也算在内
	// 计算相差的天数 % 5 的余数
	res := int(days) % 5
	if res <= 3 {
		fmt.Println("打鱼")
	} else {
		fmt.Println("晒网")
	}
}
