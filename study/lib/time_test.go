package lib

import "testing"

func TestTime15(t *testing.T) {
	Time15()
}

func TestTime16(t *testing.T) {
	Time16()
}

func TestTime17(t *testing.T) {
	Time17("2022-09-08 20:00:00")
	Time17("2022-09-07 20:00:00")
	Time17("2022-09-06 20:00:00")
	Time17("2022-09-05 20:00:00")
	Time17("2022-09-04 20:00:00")
	Time17("2022-09-03 20:00:00")
}
