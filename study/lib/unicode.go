/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/1 19:45
 */

package lib

import (
	"fmt"
	"unicode"
)

// Hanzi 判断字符串中汉字说的数量
func Hanzi(s string) (num int) {
	// 遍历s的每一个字符
	for _, c := range s {
		// unicode.Is(指定类型,字符) : 判断字符是不是指定类型,返回bool值
		if unicode.Is(unicode.Han, c) {
			num++
		}
	}
	return
}

func IsDa() {
	str := "aDC"
	for i, s := range str {
		fmt.Printf("%d %c", i, s)
		if unicode.IsUpper(s) {
			fmt.Println("是大写")
		} else if unicode.IsLower(s) {
			fmt.Println("是小写")
		}
	}
}

func IsDa2() {
	str := "aDC"
	for i, s := range str {
		fmt.Printf("%d %c ", i, s)
		if s >= 'a' {
			fmt.Println("是小写")
		} else if s >= 'A' {
			fmt.Println("是大写")
		}
	}
}

/** 测试方法
func TestHanzi() {
	fmt.Println(lib.Hanzi("hello你好呀!@@!"))  // 3
	fmt.Println(lib.Hanzi("@!#@!$@"))       // 0
	fmt.Println(lib.Hanzi("guess a guess")) // 0
}
*/
