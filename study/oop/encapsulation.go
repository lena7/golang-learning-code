/**
 * @Author: lena
 * @Description:封装
 * @Version: 1.0.0
 * @Date: 2021/8/25 12:51
 */

package oop

import "fmt"

// 封装teacher对象：首字母大写其他包也能访问到
type Teacher struct {
	// 属性开头如果是小写，就是私有属性
	Tid  int
	Name string
}

// 在func后面用this绑定当前对象 必须使用指针 不然的话this只是当前对象的一个副本
func (this *Teacher) SetTid(tid int) {
	this.Tid = tid
}

// 首字母大写的方法能够被外界访问
func (this *Teacher) GetTid() int {
	return this.Tid
}

func (this *Teacher) SetName(name string) {
	this.Name = name
}

func (this *Teacher) GetName() string {
	return this.Name
}

func (this *Teacher) Print() {
	fmt.Println(this.Tid, this.Name)
}

func Encapsulation() {
	// 创建对象
	teacher1 := Teacher{Tid: 1001, Name: "lena"}
	teacher1.Print() // 1001 lena
	// 获取值
	s := teacher1.GetName()
	fmt.Println(s) // lena
	// 修改值
	teacher1.SetName("golang")
	teacher1.Print() // 1001 golang
}
