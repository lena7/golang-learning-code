/**
 * @Author: lena
 * @Description:继承
 * @Version: 1.0.0
 * @Date: 2021/8/25 13:22
 */

package oop

import "fmt"

// 父类
type Human struct {
	Name string
	Age  int
}

func (this *Human) Action() {
	fmt.Println("human action ...")
}

func (this *Human) Show() {
	fmt.Println("human show ...")
}

func (this *Human) Print() {
	fmt.Println(this.Name, this.Age)
}

type Woman struct {
	Human  // 将父类的类名写到子类的属性中：继承Human
	talent string
}

// 重写父类方法
func (this *Woman) Action() {
	fmt.Println("woman action ...")
}

// 子类添加新方法
func (this *Woman) Dance() {
	fmt.Println("woman dance ...")
}

func Extend() {
	human := Human{Name: "human", Age: 18}
	human.Action() // human action ...
	human.Show()   // human show ...
	human.Print()  // human 18
	fmt.Println("---------------")
	woman := Woman{talent: "dance"}
	woman.Name = "woman" // 继承了父类的属性方法
	woman.Age = 20
	woman.Print()  // woman 20
	woman.Action() // woman action ...
	woman.Dance()  // woman dance ...
	woman.Show()   // human show ...
}
