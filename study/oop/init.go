package oop

import (
	"fmt"
)

func init() {
	fmt.Println("init")
}

func Init() { // 大写不默认执行
	fmt.Println("Init")
}

func T1() {
	fmt.Println("start")
}
