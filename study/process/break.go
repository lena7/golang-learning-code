/**
 * @Author: lena
 * @Description:break学习
 * @Version: 1.0.0
 * @Date: 2021/9/13 22:09
 */

package process

import "fmt"

// break到某一个标签处：可在多层循环的时候使用
func BreakToTag() int {
	res := 0
tag1:
	for i := 0; i < 10; i++ {
		//tag2:
		for j := 0; j < 10; j++ {
			if i+j == 18 {
				res = i + j
				break tag1
			}
			res = i + j
		}
	}
	return res
}

/** *计算能经过几个路口
  某人有100,000元 每经过一个路口需要交费 交费规则如下
      1.当现金>50000时，每次交5%
      2.当现金<=50000时，每次交1000
  求：该人可以经过多少次路口
*/
func CalCrossNum() {
	money := 100000
	sum := 0
	for {
		if money <= 0 {
			break
		}
		if money > 50000 {
			money -= money * 5 / 100
		} else {
			money -= 1000
		}
		sum++
	}
	fmt.Println("可以经过", sum, "个路口")
}
