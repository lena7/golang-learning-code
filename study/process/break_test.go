/**
 * @Author: lena
 * @Description:break的测试类
 * @Version: 1.0.0
 * @Date: 2021/9/13 22:12
 */

package process

import (
	"reflect"
	"testing"
)

func TestBreakToTag(t *testing.T) {
	res := BreakToTag()
	if !reflect.DeepEqual(res, 18) {
		t.Errorf("error")
	}
}
