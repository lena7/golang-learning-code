/**
 * @Author: lena
 * @Description:continue
 * @Version: 1.0.0
 * @Date: 2021/9/13 22:14
 */

package process

import (
	"fmt"
)

// *打印1-100内的奇数：for+continue
func PrintOddNum() {
	for i := 1; i <= 100; i++ {
		if i%2 == 0 {
			continue
		}
		fmt.Println(i)
	}
}

// *键盘输入不确定数量的整数 判断正数和负数的个数 直到输入0时退出程序
func GetInputNPNum() {
	p := 0
	n := 0
	cur := 0
	fmt.Println("在下方输入任意整数，直到输入0会退出：")
	for {
		fmt.Scanln(&cur)
		if cur == 0 {
			break
		}
		if cur > 0 {
			p++
		} else {
			n++
		}
	}
	fmt.Printf("正数个数：%v；负数个数：%v。\n", p, n)
}

// *循环实现 获取输入月份的天数 若输入不合法数据 使用continue
func GetMonthDays() {
	m := map[int]int{
		1: 31,
		// 二月可能是29天也可能是28天
		3:  31,
		4:  30,
		5:  31,
		6:  30,
		7:  31,
		8:  31,
		9:  30,
		10: 31,
		11: 30,
		12: 31,
	}
	for {
		fmt.Printf("请输入你想要查询的年份：")
		year := 0
		fmt.Scanln(&year)
		fmt.Printf("请输入你想查询的月份：")
		month := 0
		fmt.Scanln(&month)
		if month < 1 || month > 12 {
			fmt.Println("不存在该月份")
			continue
		}
		if month == 2 {
			if year%4 == 0 {
				fmt.Println("该月份共有29天")
			} else {
				fmt.Println("该月份共有28天")
			}
		} else {
			fmt.Printf("该月份共有%v天\n", m[month])
		}
	}
}
