/**
 * @Author: lena
 * @Description:defer
 * @Version: 1.0.0
 * @Date: 2021/8/24 21:47
 */

package process

import "fmt"

func Defer1() {
	defer fmt.Println("defer")
	fmt.Println("last")
	// 先输出last，最后输出defer
}

func method3() int {
	defer fmt.Println("defer") // 第二个执行：defer
	return returndata()
}

func returndata() int {
	fmt.Println("return") // 第一个执行：return
	return 10
}

func Defer2() {
	a := method3()
	fmt.Println(a) // 第三个执行：10
}

// Defer3 defer采用压栈方式 先defer的后执行
func Defer3() {
	fmt.Println("start")
	defer fmt.Println("defer1")
	defer fmt.Println("defer2")
}
