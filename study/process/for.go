/**
 * @Author: lena
 * @Description:for
 * @Version: 1.0.0
 * @Date: 2021/8/30 13:10
 */

package process

import (
	"fmt"
)

// For1 for循环
func For1() {
	// 可以不用括号
	for i := 0; i < 4; i++ {
		fmt.Println(i)
	}
}

// For2 for...range : 相当于java的foreach
func For2() {
	maps := map[int]string{
		1: "lena",
		2: "jazh",
	}
	for index, value := range maps {
		fmt.Println(index, value)
	}
}

// For3 遍历字符串
func For3() {
	str := "4+5=?"
	for _, s := range str {
		fmt.Printf("%T\n", s) // int32
		fmt.Println(s, string(s))
	}
}

type student struct {
	age  int
	name string
}

func (s *student) set(age int, name string) {
	s.age = age
	s.name = name
}

// For4 在for循环中改变结构体值 直接调用属性的话实际只改变了当前循环内局部变量的值 实际结构体数组中的值没有改变
func For4() {
	s := []student{{12, "lena"}, {12, "lena"}}
	fmt.Println(s) // [{12 lena} {12 lena}]
	for _, t := range s {
		t.age = 100
		t.name = "nihao"
	}
	fmt.Println(s) // [{12 lena} {12 lena}]
	for _, t := range s {
		t.set(200, "hello")
	}
	fmt.Println(s) // [{12 lena} {12 lena}]
	for index, _ := range s {
		s[index].set(300, "hello")
	}
	fmt.Println(s) // [{300 hello} {300 hello}]
}

// For5 单参数
func For5() {
	str := []string{"a", "b", "c", "d"}
	for s := range str {
		fmt.Printf("%v ", s)
	}
	fmt.Println()
}

// SumClassAvg *统计3个班的成绩情况，每个班有5名同学，求出各个班的平均分和所有班级的平均分（学生成绩从键盘输入）
func SumClassAvg() {
	total := 0
	for i := 1; i < 4; i++ {
		sum := 0
		fmt.Printf("请根据提示依次输入%v班学生的成绩\n", i)
		for j := 1; j < 6; j++ {
			cur := 0
			fmt.Printf("请输入第%v个学生的成绩：\n", j)
			fmt.Scanln(&cur)
			sum += cur
		}
		f := float32(sum) / 5.00
		fmt.Printf("班级%v的总分：%v,平均成绩：%v\n", i, sum, f)
		total += sum
	}
	avg := float32(total) / 15.00
	fmt.Printf("所有班级的总分：%v,平均成绩：%v\n", total, avg)
}

// PassNum *统计三个班及格人数，每个班有五名同学
func PassNum() {
	pass := 0
	for i := 1; i < 4; i++ {
		sum := 0
		fmt.Printf("请根据提示依次输入%v班学生的成绩\n", i)
		for j := 1; j < 6; j++ {
			cur := 0
			fmt.Printf("请输入第%v个学生的成绩：\n", j)
			fmt.Scanln(&cur)
			if cur >= 60 {
				sum++
			}
		}
		pass += sum
		fmt.Printf("班级%v的及格人数为：%v人\n", i, sum)
	}
	fmt.Printf("总共及格人数有：%v人", pass)
}

// PrintPyramid *打印金字塔，输入层数
func PrintPyramid(layer int) {
	// 金字塔：每一层*数量按[1,3,5,7,9,11...]递增
	// 1.根据层数计算每行应该有几格 : 层数+层数-1
	sum := layer + layer - 1
	// 2.遍历打印每一层
	for i := 1; i <= layer; i++ {
		// 2.1 计算当前层应该有几个*,有几个空格
		cur := i + i - 1
		blank := (sum - cur) / 2
		// 2.2 在*前打印空格
		for j := 0; j < blank; j++ {
			fmt.Printf(" ")
		}
		// 2.3 打印*
		for j := 0; j < cur; j++ {
			fmt.Printf("*")
		}
		// 2.4 *后打印空格
		for j := 0; j < blank; j++ {
			fmt.Printf(" ")
		}
		// 2.5 换行
		fmt.Println()
	}
}
