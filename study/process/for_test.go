/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/13 21:30
 */

package process

import "testing"

func TestFor4(t *testing.T) {
	For4()
}

func TestPrintPyramid(t *testing.T) {
	// PrintPyramid(3)
	PrintPyramid(6)
}

func TestFor5(t *testing.T) {
	For5()
}
