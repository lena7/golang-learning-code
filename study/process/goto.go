/**
 * @Author: lena
 * @Description:goto
 * @Version: 1.0.0
 * @Date: 2021/9/6 21:00
 */

package process

import "fmt"

// 退出双重循环
func Goto1() {
	for i := 0; i < 100; i++ {
		for j := 0; j < 10; j++ {
			if i == 30 {
				// 设置退出标签
				goto out
			}
			//fmt.Println(i, j)
		}
	}
	return
	// 标签
out:
	fmt.Println("成功退出for循环")
}
