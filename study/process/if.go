/**
 * @Author: lena
 * @Description:if
 * @Version: 1.0.0
 * @Date: 2021/8/29 11:52
 */

package process

import "fmt"

// if可以包含一个初始化表达式
func If1() {
	if num := 0; num == 0 {
		fmt.Println("yes")
	}
}
