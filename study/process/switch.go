/**
 * @Author: lena
 * @Description:switch
 * @Version: 1.0.0
 * @Date: 2021/9/6 12:11
 */

package process

import "fmt"

func Switch1() {
	i := 10
	switch i {
	case 1:
		fmt.Println("是1")
	case 10:
		fmt.Println("是10")
	case 100:
		fmt.Println("不是100")
	}
	// 是10
}

// 只会执行一个case,满足一个后后续不会执行
func Switch2() {
	i := 10
	switch {
	case i > 1:
		fmt.Println("是1")
	case i >= 10:
		fmt.Println("是10")
	case i > 100:
		fmt.Println("不是100")
	}
	// 是1
}

// fallthrough：可以执行满足条件的case的下一个case，是为了兼容C语言中的case设计的。
func Switch3() {
	s := "a"
	switch {
	case s == "a":
		fmt.Println("a")
		fallthrough
	case s == "b":
		fmt.Println("b")
	case s == "c":
		fmt.Println("c")
	default:
		fmt.Println("...")
	}
	// 输出 a，b
}

// case可以取范围
func Switch4() {
	for {
		var a string
		fmt.Printf("请输入一个字符：")
		fmt.Scanln(&a)
		switch { // 适用范围的不要指定字符
		case a >= "a" && a <= "z":
			fmt.Println(a, "是小写字母")
		case a >= "A" && a <= "Z":
			fmt.Println(a, "是大写字母")
		case a >= "0" && a <= "9":
			fmt.Println(a, "是数字")
		default:
			fmt.Println(a, "是特殊字符")
		}
	}
	{
	}
}
