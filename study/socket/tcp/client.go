/**
 * @Author: lena
 * @Description:tcp客户端
 * @Version: 1.0.0
 * @Date: 2021/9/9 12:51
 */

package tcp

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

// 客户端

func Client1() {
	// 建立连接
	conn, err := net.Dial("tcp", "127.0.0.1:9000")
	if err != nil {
		fmt.Println("client conn err :", err)
		return
	}
	defer conn.Close()
	// 输入要发送的数据
	fmt.Println("请输入你要发送的信息：")
	reader := bufio.NewReader(os.Stdin)
	for {
		// 读取用户输入的信息
		readString, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("client read err :", err)
			break
		}
		fmt.Println(readString)
		// 发送用户输入信息给server端
		write, err := conn.Write([]byte(readString))
		if err != nil {
			fmt.Println("client write err :", err)
			break
		}
		// 读取数据
		for {
			buf := [1024]byte{}
			read, err := conn.Read(buf[:write])
			if err != nil {
				fmt.Println("client read err :", err)
				break
			}
			fmt.Println("client receive :", string(buf[:read]))
		}
	}
}
