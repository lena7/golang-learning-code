/**
 * @Author: lena
 * @Description:tcp服务端
 * @Version: 1.0.0
 * @Date: 2021/9/8 18:31
 */

package tcp

import (
	"bufio"
	"fmt"
	"net"
)

// 服务端

// tcp通信：http://39.96.59.198:9000/
func Server1() {
	// 服务端监听端口
	// func Listen(network, address string) (Listener, error)
	listen, err := net.Listen("tcp", "127.0.0.1:9000")
	if err != nil {
		fmt.Println("server listen err :", err)
		return
	}
	defer listen.Close()
	// 建立链接
	conn, err := listen.Accept()
	if err != nil {
		fmt.Println("accept err :", err)
		return
	}
	defer conn.Close()
	// 接收数据
	reader := bufio.NewReader(conn)
	readString, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("read err :", err)
		return
	}
	fmt.Println("server receive:", readString)
	_, err = conn.Write([]byte("service receive" + readString))
	if err != nil {
		fmt.Println("server answer err :", err)
		return
	}

}
