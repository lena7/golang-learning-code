package main

import (
	"encoding/json"
	"fmt"
)

// 思考: 如何在结构体内加入不确定的key,然后进行序列化

type def struct {
	Name  string
	Value string
}

func main() {
	d := def{Name: "123", Value: "123"}
	str := `{"Tag":"custom","Id":1,"Name":"1234"}`

	dataMap := map[string]interface{}{}
	dByte, err := json.Marshal(d)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(dByte, &dataMap)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal([]byte(str), &dataMap)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", dataMap) // map[Id:1 Name:1234 Tag:custom Value:123]

	dataByte, err := json.Marshal(dataMap)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%s\n", dataByte) // {"Id":1,"Name":"1234","Tag":"custom","Value":"123"}
}
