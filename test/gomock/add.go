package gomock

// MysqlClient 假设是第三方依赖
type MysqlClient interface {
	Add(int) int
}

func Add(client MysqlClient, a int) int {
	return client.Add(a)
}
