package gomock

import (
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
)

func TestAdd(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()
	client := NewMockMysqlClient(ctl)
	// 此时执行Add方法已经不用输入client对象
	// 期望执行Add方法输入1的时候返回2
	client.EXPECT().Add(1).Return(2)
	// 传入mock执行
	x := Add(client, 1)
	if x != 2 {
		fmt.Println(x)
		t.Fatal() //报错
	}
}

func TestAdd2(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()
	client := NewMockMysqlClient(ctl)
	// gomock.Eq() 强制要求输入值是100
	client.EXPECT().Add(gomock.Eq(100)).Return(101)
	// 传入mock执行
	Add(client, 100)
}

func TestAdd3(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()
	client := NewMockMysqlClient(ctl)
	// 强制要求输入任何值gomock.Any()都返回值是2
	client.EXPECT().Add(gomock.Any()).Return(2)
	// 传入mock执行
	x := Add(client, 3)
	fmt.Println(x)
	if x != 2 {
		t.Fatal()
	}
}

func TestAdd4(t *testing.T) {
	tests := []struct {
		name     string
		getParam func() (int, int)
		stub     func(ctl *gomock.Controller) MysqlClient
	}{
		{
			name: "期望执行Add方法输入1的时候返回2",
			getParam: func() (int, int) {
				return 1, 2
			},
			stub: func(ctl *gomock.Controller) MysqlClient {
				client := NewMockMysqlClient(ctl)
				client.EXPECT().Add(1).Return(2)
				return client
			},
		}, {
			name: "强制要求输入值是100",
			getParam: func() (int, int) {
				return 100, 101 // 输入非100会报错
			},
			stub: func(ctl *gomock.Controller) MysqlClient {
				client := NewMockMysqlClient(ctl)
				client.EXPECT().Add(gomock.Eq(100)).Return(101)
				return client
			},
		}, {
			name: "强制要求输入任何值gomock.Any()都返回值是2",
			getParam: func() (int, int) {
				return 3, 2 // 输出是非2会报错
			},
			stub: func(ctl *gomock.Controller) MysqlClient {
				client := NewMockMysqlClient(ctl)
				client.EXPECT().Add(gomock.Any()).Return(2)
				return client
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctl := gomock.NewController(t)
			client := test.stub(ctl)
			in, out := test.getParam()
			res := Add(client, in)
			if res != out {
				t.Fatal(test.name)
			}
		})
	}
}
