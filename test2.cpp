
unsigned int ConvertAgentIdToInt(const string& strId) {
    unsigned int nId = 0;
    string strIp;
    if (IpStringToInt(nId, strId) == 0)
        return nId;

    int ret = EthnetGetIpAddress(strIp, strId);
    if (ret == 0 && !strIp.empty()) {
        if (IpStringToInt(nId, strIp) != 0)
            nId = 0;
    }
    else {
        LogWarning("ReLoadCfg(EthnetGetIpAddress is return error, we try num of agent id, errno: %d)", ret);
        nId = StringToInt(strId);
    }
    EthnetInfoDestroy();

    return nId;
}
