/**
 * @Author: lena
 * @Description:实现cat命令
 * @Version: 1.0.0
 * @Date: 2021/9/6 11:52
 */

package filex

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

// Cat 实现cat命令: cat path 读取文件输出到终端
func Cat(path string) {
	open, err := os.Open(path)
	if err != nil {
		fmt.Println("open err :", err)
		return
	}
	reader := bufio.NewReader(open)
	for {
		// 按行读取
		readString, err := reader.ReadString('\n')
		if err == io.EOF {
			fmt.Fprintln(os.Stdout, readString)
			return
		}
		if err != nil {
			fmt.Println("read err :", err)
			return
		}
		// os.Stdout表示控制台
		fmt.Fprintln(os.Stdout, readString)
	}
}
