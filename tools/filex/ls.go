package filex

import (
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
)

// Ls 获取path目录文件夹下所有文件名 不包括子目录
func Ls(path string) {
	fileList, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}
	for i := range fileList {
		fmt.Println(fileList[i].Name())
	}
}

// LsComplete 查询文件夹下文件 获取全路径
func LsComplete(path string) {
	// 查询文件的时候加上path=>全路径
	fileList, err := filepath.Glob(filepath.Join(path, "*"))
	if err != nil {
		log.Fatal(err)
	}
	for i := range fileList {
		fmt.Println(fileList[i])
	}
}
