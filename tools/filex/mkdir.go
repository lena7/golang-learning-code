package filex

import (
	"fmt"
	"os"
)

// Mkdir 创建文件夹 若存在则比创建
func Mkdir(path string) {
	exists, err := PathExists(path)
	if err != nil {
		fmt.Println("PathExistsErr:", err)
		return
	}
	if !exists {
		err := os.Mkdir(path, os.ModePerm)
		if err != nil {
			fmt.Println("mkdirErr:", err)
			return
		}
	}
	fmt.Println("success")
}
