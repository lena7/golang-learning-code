/**
 * @Author: lena
 * @Description:打开文件
 * @Version: 1.0.0
 * @Date: 2021/9/6 11:52
 */

package filex

import (
	"fmt"
	"os"
)

// Open1 只读
func Open1() {
	file, err := os.OpenFile("./exercise/file/test.txt", os.O_RDONLY, 0644)
	if err != nil {
		fmt.Println("open err :", err)
		return
	}
	fmt.Println(file.Name()) // ./exercise/file/test.txt
}

// Open2 若没文件自己创建,可写
func Open2() {
	file, err := os.OpenFile("./exercise/file/test1.txt", os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println("open err :", err)
		return
	}
	defer file.Close()
	_, err2 := file.Write([]byte("hello lena,i create"))
	if err2 != nil {
		fmt.Println("write err :", err)
		return
	}
}
