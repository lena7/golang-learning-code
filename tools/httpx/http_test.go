package httpx

import (
	"fmt"
	"testing"
)

func TestGet(t *testing.T) {
	response := Get("https://www.baidu.com/")
	fmt.Println(response) // 返回百度页面 即页面的html代码
}
