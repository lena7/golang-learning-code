package slice

// 删除数组中的元素
// 使用comparable限制T只能为可比较类型,这样才能使用`==`进行判断
func Remove[T comparable](data []T, target T) (int, []T) {
	for index, val := range data {
		// 存在要删除的元素
		if val == target {
			return index, append(data[:index], data[index+1:]...)
		}
	}
	// 不存在要删除的元素
	return -1, data
}
