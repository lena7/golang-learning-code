package slice

import "testing"

func TestRemove(t *testing.T) {
	type c struct {
		name    string
		data    []string
		target  string
		resIdx  int
		resData []string
	}

	cases := []c{
		{name: "删除元素不存在", data: []string{"1", "2"}, target: "3", resIdx: -1, resData: []string{"1", "2"}},
		{name: "删除元素存在", data: []string{"1", "2", "3"}, target: "2", resIdx: 1, resData: []string{"1", "3"}},
		{name: "删除元素存在第一个", data: []string{"1", "2", "3"}, target: "1", resIdx: 0, resData: []string{"2", "3"}},
		{name: "删除元素存在最后一个", data: []string{"1", "2", "3"}, target: "3", resIdx: 2, resData: []string{"1", "2"}},
	}

	for _, item := range cases {
		idx, data := Remove(item.data, item.target)
		if idx != item.resIdx || !equal(data, item.resData) {
			t.Errorf("case(%+v) get error output(%d,%s)", item, idx, data)
			continue
		}
	}
}

func equal(s1, s2 []string) bool {
	if len(s1) != len(s2) {
		return false
	}
	for idx, _ := range s1 {
		if s1[idx] != s2[idx] {
			return false
		}
	}
	return true
}
