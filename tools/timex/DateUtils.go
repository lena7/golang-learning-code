/**
 * @Author: lena
 * @Description:时间工具类
 * @Version: 1.0.0
 * @Date: 2021/9/14 16:59
 */

package timex

import (
	"fmt"
	"time"
)

const (
	DefaultLayout = "2006-01-02 15:04:05"
)

func ParseTime(t string) (time.Time, error) {
	return time.Parse(DefaultLayout, t)
}

// FormatTime 格式化输出日期：2021-9-14 17:3:52
func FormatTime(t time.Time) string {
	res := fmt.Sprintf("%d-%d-%d %d:%d:%d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
	return res
}

// FormatNow 格式化当前时间 2021-9-14 17:3:52
func FormatNow() string {
	t := time.Now()
	res := fmt.Sprintf("%d-%d-%d %d:%d:%d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
	return res
}

// FormatNowTime 格式化当前时间 返回time.Time类型
func FormatNowTime() time.Time {
	s := time.Now().Format(DefaultLayout)
	loc, _ := time.LoadLocation("Local")
	// 如果直接使用time.Parse方法,不会设置当前时区为东八区,存入数据库的时候会出现时间不一致的问题
	t, err := time.ParseInLocation(DefaultLayout, s, loc)
	fmt.Println(s)
	if err != nil {
		panic(err)
	}
	return t
}

// StringToTime 将传入的字符串t转换为time.Time类型
func StringToTime(t string) time.Time {
	loc, _ := time.LoadLocation("Local")
	s, err := time.ParseInLocation(DefaultLayout, t, loc)
	if err != nil {
		panic(err)
	}
	return s
}

// GetFirstDateOfMonth 获取传入的时间所在月份的第一天，即某月第一天的0点
func GetFirstDateOfMonth(d time.Time) time.Time {
	d = d.AddDate(0, 0, -d.Day()+1)
	return GetZeroTime(d)
}

// GetLastDateOfMonth 获取传入的时间所在月份的最后一天，即某月最后一天的0点
func GetLastDateOfMonth(d time.Time) time.Time {
	return GetFirstDateOfMonth(d).AddDate(0, 1, -1)
}

// GetZeroTime 获取某一天的0点时间
func GetZeroTime(d time.Time) time.Time {
	return time.Date(d.Year(), d.Month(), d.Day(), 0, 0, 0, 0, d.Location())
}

// FormatYYMMDD 格式化时间 YY/MM/DD
func FormatYYMMDD(d time.Time) string {
	t := d.Format("06/01/02")
	return t
}

// GetBetweenDates 根据开始日期和结束日期计算出时间段内所有日期 输入日期格式yyyy-MM-dd 返回日期格式yyyy-MM-dd 09:00:00
func GetBetweenDates(sdate, edate string) []string {
	d := []string{}
	timeFormatTpl := "2006-01-02 15:04:05"
	if len(timeFormatTpl) != len(sdate) {
		timeFormatTpl = timeFormatTpl[0:len(sdate)]
	}
	date, err := time.Parse(timeFormatTpl, sdate)
	if err != nil {
		// 时间解析，异常
		return d
	}
	date2, err := time.Parse(timeFormatTpl, edate)
	if err != nil {
		// 时间解析，异常
		return d
	}
	if date2.Before(date) {
		// 如果结束时间小于开始时间，异常
		return d
	}
	// 输出日期格式固定
	timeFormatTpl = "2006-01-02 09:00:00"
	date2Str := date2.Format(timeFormatTpl)
	d = append(d, date.Format(timeFormatTpl))
	for {
		date = date.AddDate(0, 0, 1)
		dateStr := date.Format(timeFormatTpl)
		d = append(d, dateStr)
		if dateStr == date2Str {
			break
		}
	}
	return d
}

// FormatTimeZero 时分秒清零
func FormatTimeZero(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}
