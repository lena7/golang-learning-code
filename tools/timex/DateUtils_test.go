/**
 * @Author: lena
 * @Description:
 * @Version: 1.0.0
 * @Date: 2021/9/14 17:05
 */

package timex

import (
	"fmt"
	"testing"
	"time"
)

func TestFormatTime(t *testing.T) {
	formatTime := FormatTime(time.Now())
	fmt.Printf("输出yyyy-MM-dd格式的日期：")
	fmt.Println(formatTime) // 2021-9-14 17:5:54
}

func TestFormatYYMMDD(t *testing.T) {
	fmt.Println(FormatYYMMDD(time.Now()))
}

func TestGetBetweenDates(t *testing.T) {
	dates := GetBetweenDates("2021-12-28", "2022-01-24")
	fmt.Println(dates)
}

func TestGetBetweenDat1es(t *testing.T) {
	data1 := FormatTimeZero(time.Now().AddDate(0, 0, -1))
	data2 := FormatTimeZero(time.Now())
	fmt.Println(data1.String(), data2)
	format := data1.Format("2006-01-02 15:04:05")
	fmt.Println(format)
}
