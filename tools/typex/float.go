package typex

import (
	"fmt"
	"math"
	"strconv"
)

// TurnFloat64 截取f的小数 prec 代表小数位数 tip不会四舍五入
func TurnFloat64(f float64, prec int) float64 {
	x := math.Pow10(prec)
	return math.Trunc(f*x) / x
}

// CutFloat6 截取prec位小数 同时保证四舍五入
func CutFloat6(f float64) float64 {
	res := fmt.Sprintf("%.6f", f)
	return StrToFloat(res)
}

// FloatToStr float64->string
func FloatToStr(num float64) string {
	return strconv.FormatFloat(num, 'f', -1, 64)
}

// FloatToStrFmt float64->string
func FloatToStrFmt(num float64) string {
	return fmt.Sprintf("%f", num)
}

// StrToFloat 将字符串转换和浮点数
func StrToFloat(str string) float64 {
	f, err := strconv.ParseFloat(str, 64)
	if err != nil {
		panic(err)
	}
	return f
}

// FloatToStrFmt2 float64->string 保留2位小数
func FloatToStrFmt2(num float64) string {
	return fmt.Sprintf("%.2f", num)
}

// FloatToInt float -> int
func FloatToInt(data float64) int64 {
	str := strconv.FormatFloat(data, 'f', -1, 64)
	res, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return -1
	}
	return res
}
