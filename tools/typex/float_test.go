package typex

import (
	"fmt"
	"testing"
)

func TestFloatToInt(t *testing.T) {
	fmt.Println(FloatToInt(100.0))  // 100
	fmt.Println(FloatToInt(92.0))   // 92
	fmt.Println(FloatToInt(70.1))   // -1
	fmt.Println(FloatToInt(100.00)) // 100
	fmt.Println(FloatToInt(100))    // 100
}

func TestFloatToStrFmt2(t *testing.T) {
	fmt.Println(FloatToStrFmt2(100))
	fmt.Println(FloatToStrFmt2(39.1))
}

func TestTurnFloat64(t *testing.T) {
	f1 := 100.0000
	f2 := 92.4293346
	fmt.Println(TurnFloat64(f1, 6)) // 100
	fmt.Println(TurnFloat64(f2, 6)) // 92.429334
	// 不会四舍五入
	fmt.Println(TurnFloat64(100.0221335, 6)) // 100.022133
	fmt.Println(TurnFloat64(100.0221337, 6)) // 100.022133
}

func TestCutFloat64(t *testing.T) {
	fmt.Println(CutFloat6(100.0221333))    // 100.022133
	fmt.Println(CutFloat6(100.0221335))    // 100.022133
	fmt.Println(CutFloat6(100.0221337))    // 100.022134
	fmt.Println(CutFloat6(100.0221345))    // 100.022135
	fmt.Println(CutFloat6(0.992555555555)) // 0.992556
	fmt.Println(CutFloat6(100))            // 100
	fmt.Println(CutFloat6(0))              // 0
}

func TestFloatToStr(t *testing.T) {
	fmt.Println(FloatToStr(99.93323))    // 99.93323
	fmt.Println(FloatToStr(99.0000))     // 99
	fmt.Println(FloatToStr(100.0221335)) // 100.0221335
	fmt.Println(FloatToStr(100.0221337)) // 100.0221337
	fmt.Println(FloatToStr(100.0221357)) // 100.0221357
}

func TestFloatToString(t *testing.T) {
	fmt.Println(FloatToStrFmt(99.93323)) // 99.933230
	fmt.Println(FloatToStrFmt(99.0000))  // 99.000000
	fmt.Println(FloatToStrFmt(99.99))    // 99.990000
}

func TestFloatToInt2(t *testing.T) {
	var f1 float64
	var f2 float64
	f1 = -1.0
	f2 = -1
	if f1 == f2 {
		fmt.Println("1")
	} else {
		fmt.Println("0`")
	}
}
