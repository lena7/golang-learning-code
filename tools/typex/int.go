/**
 * @Author: lena
 * @Date: 2021/10/15 15:31
 * @Description: 类型转换
 * @Version: 1.0.0
 */

package typex

import (
	"fmt"
	"strconv"
)

// Turn1 int <-> int32
func Turn1() {
	var a int
	var b int32
	a = 4
	b = int32(a)
	fmt.Println(b)
}

// Int32ToInt int32 -> int
func Int32ToInt(a int32) (int, error) {
	b := string(a)
	c, err := strconv.Atoi(b)
	return c, err
}

// IntToString int -> string
func IntToString(a int) string {
	return fmt.Sprintf("%d", a)
}
