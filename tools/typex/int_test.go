/**
 * @Author: lena
 * @Date: 2021/10/15 15:33
 * @Description: turnType测试方法
 * @Version: 1.0.0
 */

package typex

import (
	"fmt"
	"testing"
)

func Test(t *testing.T) {
	Turn1()
}

func TestTurn2(t *testing.T) {
	str := "4523"
	for _, s := range str {
		res, err := Int32ToInt(s)
		fmt.Println(res, err)
	}
}

func TestIntToString(t *testing.T) {
	fmt.Println(IntToString(2))
	fmt.Println(IntToString(10))
}
