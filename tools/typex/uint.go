package typex

import (
	"strconv"
)

// StrToUint32 string -> uint32
func StrToUint32(data string) (uint32, error) {
	// string -> int
	res, err := strconv.Atoi(data)
	if err != nil {
		return 0, err
	}
	// int -> uint32
	return uint32(res), nil
}
