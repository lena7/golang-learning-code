package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// 模拟db
var db = make(map[string]string)

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()

	// Get请求 不带参
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong") // code,data
	})

	// Get请求 带参 （将/user/后的value赋给key=name）
	r.GET("/user/:name", func(c *gin.Context) {
		user := c.Params.ByName("name") // 获取key=name的值
		value, ok := db[user]           // 模拟数据库查询操作
		if ok {
			c.JSON(http.StatusOK, gin.H{"user": user, "value": value})
		} else {
			c.JSON(http.StatusOK, gin.H{"user": user, "status": "no value"})
		}
	})

	// 满足以下user、password的验证才能成功请求
	authorized := r.Group("/", gin.BasicAuth(gin.Accounts{
		"foo":  "bar", // user:foo password:bar
		"manu": "123", // user:manu password:123
	}))

	/*
			curl -X POST \
		  	http://localhost:8080/admin \
		  	-H 'authorization: Basic Zm9vOmJhcg==' \
		  	-H 'content-type: application/json' \
		  	-d '{"value":"bar"}'
	*/
	authorized.POST("admin", func(c *gin.Context) {
		user := c.MustGet(gin.AuthUserKey).(string)

		// Parse JSON
		var json struct {
			Value string `json:"value" binding:"required"`
		}

		if c.Bind(&json) == nil {
			db[user] = json.Value
			c.JSON(http.StatusOK, gin.H{"status": "ok"})
		}
	})

	return r
}

func main() {
	r := setupRouter()
	// Listen and Server in 0.0.0.0:8080
	r.Run(":8080")
}
