# 快速入门
Go版本要求：Go1.9及以上

执行命令即可得到示例代码：`curl https://raw.githubusercontent.com/gin-gonic/examples/master/basic/main.go > main.go`

也可以直接启动helloworld/main.go。

启动生成的main函数，在浏览器中输入:`localhost:8080/ping`即可拿到结果。如下图所示：
![img.png](readme_img/img.png)

具体的请求接收参数，可查看**study/main.go**

## *gin.Context
对于方法`func(c *gin.Context)`中，c具有的常见方法如下：

|  方法名   | 参数  | 返回值 | 作用 |
|  ----  | ---- |  ----  | ----  |
| Query  | key string | string |获取URL中对应key的value |
| DefaultQuery  | key, defaultValue string | string | 获取URL中对应key的value,若无该key则返回defaultValue |
|  JSON  | code int, obj interface{} |    | 返回状态码code,值为JSON格式  |
|  GetRawData  |  |  []byte, error  | 获取POST请求的JSON请求体  |
|  PostForm  | key string |  string  | 获取提交表单key对应的value  |

# 上传文件
## 上传图片
代码目录：upload/picture
1. 修改main函数中commonPath的路径，即存放图片的路径
2. 启动main函数
3. 打开upload页面，选择图片后上传，就可以看到commonPath上有图片了
tip：若服务端口不是8080的话，需要修改uoload.html中路径url

# 参考文档
1. Gin快速入门：https://laravelacademy.org/post/21861
2. 李文周的博客：https://liwenzhou.com/posts/Go/Gin_framework/
3. 七米视频：https://www.bilibili.com/video/BV1gJ411p7xC?p=4&spm_id_from=pageDriver
