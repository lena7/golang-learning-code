package main

import (
	"context"
	"fmt"

	"google.golang.org/grpc"

	. "helloworld/web/rpc/grpc/simple/proto"
)

func main() {
	conn, err := grpc.Dial("127.0.0.1:8088", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	c := NewGreeterClient(conn)
	r, err := c.SayHello(context.Background(), &HelloRequest{Msg: "isme"})
	if err != nil {
		panic(err)
	}
	fmt.Println("【client】", r.Msg)
}
