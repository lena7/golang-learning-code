简单模式：服务端提供服务、客户端一次请求获取数据后返回
1. 编写proto目录下的proto文件确定服务
2. 进入到proto目录下
3. 执行命令`protoc --go_out=plugins=grpc:. helloworld.proto `
   - 其中最后跟的是proto文件，会根据该文件生成一个pb.go文件
   - 生成的pb.go文件，会包含服务的创建、客户端的创建等方法
   - 生成pb.go文件所在目录，会取决于proto文件中的go_package指定目录
4. 编写server方法、client方法
5. 执行server方法，后面每次client方法执行都能够收到返回值