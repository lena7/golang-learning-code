package main

import (
	"context"
	"net"

	"google.golang.org/grpc"

	. "helloworld/web/rpc/grpc/simple/proto"
)

type Server struct {
}

func (s *Server) SayHello(context.Context, *HelloRequest) (*HelloReply, error) {
	return &HelloReply{
		Msg: "respect",
	}, nil
}

func main() {
	g := grpc.NewServer()
	RegisterGreeterServer(g, &Server{})
	listen, err := net.Listen("tcp", "0.0.0.0:8088")
	if err != nil {
		panic(err)
	}
	err = g.Serve(listen)
	if err != nil {
		panic(err)
	}
}
