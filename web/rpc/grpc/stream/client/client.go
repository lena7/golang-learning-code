package main

import (
	"context"
	"fmt"
	"io"
	"os"
	"strconv"
	"time"

	"google.golang.org/grpc"

	"helloworld/web/rpc/grpc/stream/proto"
)

func main() {
	conn, err := grpc.Dial("127.0.0.1:9088", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	c := proto.NewGreeterClient(conn)
	//server(c)	// 服务端流模式
	client(c) // 客户端流模式
}

// 服务端流模式
func server(client proto.GreeterClient) {
	// 方法的调用根据proto文件中rpc定义的入参使用 而不是按照Server服务的定义传入
	res, err := client.ServerStream(context.Background(), &proto.RequesetData{Data: "icome"})
	if err != nil {
		panic(err)
	}
	for {
		recv, err := res.Recv()
		if err != nil {
			if err == io.EOF {
				fmt.Println("[client]读完数据啦！")
				// 退出程序
				os.Exit(1)
			}
			fmt.Println("[client]收不到数据：", err)
			break
		}
		fmt.Println("[client]收到数据：", recv) // [client]收到数据：data:"10"
	}
}

// 客户端流模式
func client(client proto.GreeterClient) {
	res, err := client.ClientStream(context.Background())
	if err != nil {
		panic(err)
	}
	i := 0
	for {
		// 调用Send方法能源源不断给客户端发送消息 不用return客户端就能够接收到消息
		err = res.Send(&proto.RequesetData{
			Data: strconv.Itoa(i),
		})
		if err != nil {
			panic(err)
		}
		// 当i>10 退出for循环 停止发送数据
		if i > 5 {
			break
		}
		i++
		// 休眠1s
		time.Sleep(time.Second)
	}
}
