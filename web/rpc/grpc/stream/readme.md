流模式：
1. 服务端流模式：客户端发送一次请求 服务端不断地发送数据流到客户端
2. 客户端流模式：客户端不断发送数据给服务端 发送结束后客户端给一个响应
3. 双向流模式（在线聊天）

快速开始
1. 编写steam.proto文件：流模式中proto文件的书写就是相对于简单模式，请求或者返回结构体使用`stream`关键词在前面定义
2. 进入stream/proto目录下
3. 执行命令：`protoc --go_out=plugins=grpc:. steam.proto`
4. 编写server、client文件
5. 执行server方法、再执行client方法