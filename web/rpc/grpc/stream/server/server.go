package main

import (
	"fmt"
	"io"
	"net"
	"os"
	"strconv"
	"time"

	"google.golang.org/grpc"

	. "helloworld/web/rpc/grpc/stream/proto"
)

// Server 定义服务
type Server struct{}

// ServerStream 服务端流模式
func (s *Server) ServerStream(req *RequesetData, res Greeter_ServerStreamServer) error {
	i := 0
	for {
		// 调用Send方法能源源不断给客户端发送消息 不用return客户端就能够接收到消息
		res.Send(&ResponseData{
			Data: strconv.Itoa(i),
		})
		i++
		// 当i>10 退出for循环 停止发送数据
		if i > 10 {
			break
		}
		// 休眠1s
		time.Sleep(time.Second)
	}
	return nil
}

// ClientStream 客户端流模式
func (s *Server) ClientStream(res Greeter_ClientStreamServer) error {
	for {
		recv, err := res.Recv()
		if err != nil {
			if err == io.EOF {
				fmt.Println("[server]读完数据啦！")
				// 退出程序
				os.Exit(1)
			}
			fmt.Println("[server]收不到数据：", err)
			break
		}
		fmt.Println("[server]收到数据：", recv)
	}
	return nil
}

func (s *Server) StreamToStream(res Greeter_StreamToStreamServer) error {
	return nil
}

func main() {
	listen, err := net.Listen("tcp", "0.0.0.0:9088")
	if err != nil {
		panic(err)
	}
	defer listen.Close()
	s := grpc.NewServer()
	RegisterGreeterServer(s, &Server{})
	// 向监听的端口提供服务
	err = s.Serve(listen)
	if err != nil {
		panic(err)
	}
}
